import itertools as it
import logging
import pandas as pd
import pronto as pt

from collections.abc import Iterator
from elasticsearch_dsl import AttrList
from helper_functions.ontology_functions import ontology, AnnotationPropertyEquivalences
from indexation.constants import APPLICATION, BIOLOGICAL_ROLE, CHEMICAL_ROLE
from indexation.data_from_postgres import CompoundInformationFromPostgreSQL
from indexation.entities import Compound, CompoundOrigins, Relation, RelationsIds, SynonymScope, Synonym


logging.getLogger("elasticsearch").setLevel(logging.WARNING)
logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO)


def classify_roles(relation: Relation, relation_id: str) -> Relation:
    """
    Classify the roles in chemical_role, biological_role or application
    :param relation: it is the current Relation object
    :param relation_id: the current processed relation
    :return: The Relation object with the correct role classifications

    If the current relation_id to be processed is 'has_role', then we verify all the parents of the role.
    We mark as True/False in each classification.
    """
    if relation_id == RelationsIds.has_role.value:
        role_parents = ontology.get_term(relation.chebi_accession).superclasses().to_set().ids
        relation.chemical_role = CHEMICAL_ROLE in role_parents
        relation.biological_role = BIOLOGICAL_ROLE in role_parents
        relation.application = APPLICATION in role_parents

    return relation


def get_relations(compound: pt.Term, relation_id: str) -> Iterator[Relation]:
    # If the current compound has relations, then these will be yield here. the relation is_a is specially managed and
    # the entities for that relation is saved in parents field. This function set the other relations (has_role,
    # has_functional_parent, etc.)
    relation = ontology.get_relationship(relation_id)
    # If compound param is for example 'water', then entities are all the nodes connected to it, taking into account the relation
    # water - has_role -> metabolite
    # water - has_role -> human_metabolite
    # water - has role -> plant_metabolite
    # Then, entities would look like TermSet({Term('metabolite'), Term('human_metabolite), Term('plant_metabolite)}) for has_role relation.
    entities = compound.relationships.get(relation, pt.TermSet())
    for entity in entities:
        r = Relation(
            id=int(entity.id.split(":")[1]), chebi_accession=entity.id, definition=entity.definition, name=entity.name
        )

        yield classify_roles(r, relation_id)

    # A compound can also inherit 'has_role' relations from its parents (it happens ONLY for 'has_role'
    # relation). Here we are generating the 'has_role' relations inherited from the parents of the current compound.
    # It is just a recursive call using each parent of the current compound as parameter.
    if relation_id == RelationsIds.has_role.value:
        for parent in compound.superclasses(with_self=False):
            if relation in parent.relationships:
                yield from get_relations(parent, relation_id)


def get_compounds(compound_info_from_pg: CompoundInformationFromPostgreSQL) -> Iterator[dict]:
    # We are going to map into elasticsearch only not-obsolete with 2 and 3 stars compounds.
    compounds = filter(lambda term: not term.obsolete and "chebi/1:STAR" not in term.subsets, ontology.terms())
    # unpacking information, we must preserve the tuple order.
    pg_basic_information, pg_compound_origins, pg_structures = compound_info_from_pg
    for compound in compounds:
        try:
            raw_id = int(compound.id.split(":")[1])
            df_basic_information: pd.DataFrame = (
                pg_basic_information.query(f"compound_id == {raw_id}")
                .drop(columns=["compound_id"])
                .reset_index(drop=True)
            )
            df_compound_origins: pd.DataFrame = pg_compound_origins.query(f"compound_id == {raw_id}").reset_index(
                drop=True
            )
            df_structures: pd.DataFrame = (
                pg_structures.query(f"compound_id == {raw_id}").drop(columns=["compound_id"]).reset_index(drop=True)
            )

            # Here we are creating the ElasticSearch Inner documents, which are parts of a Compound document.

            initial_data = {
                "chebi_accession": compound.id,
                "name": compound.name,
                "ascii_name": None
                if df_basic_information["ascii_name"].empty
                else df_basic_information.at[0, "ascii_name"],
                "alternative_ids": AttrList(compound.alternate_ids),
                "stars": next(int(i) for i in it.chain(set(compound.subsets).pop()) if i.isdigit()),
                "default_structure": None
                if df_structures["default_structure"].empty
                else df_structures.at[0, "default_structure"].item(),
                "definition": compound.definition,
                "database_references": AttrList((xref.id for xref in compound.xrefs)),
                "modified_on": None
                if df_basic_information["modified_on"].empty
                else df_basic_information.at[0, "modified_on"],
            }

            annotations = {
                AnnotationPropertyEquivalences[annotation.property.split("/")[-1]].value: int(annotation.literal)
                if "integer" in annotation.datatype
                else float(annotation.literal)
                if "decimal" in annotation.datatype
                else annotation.literal
                for annotation in compound.annotations
            }

            synonyms = {
                "synonyms": [
                    Synonym(
                        name=synonym.description,
                        scope=SynonymScope[synonym.scope],
                        database_reference=xref.id,
                        type=(synonym.type.description.strip().replace(" ", "_") if synonym.type else None),
                    )
                    for synonym in compound.synonyms
                    for xref in synonym.xrefs
                ]
            }

            structures = {
                "structures": None if df_structures["structures"].empty else AttrList(df_structures.at[0, "structures"])
            }

            compound_origins = {
                "compound_origins": None
                if df_compound_origins.isnull().all(axis=None)
                else [
                    CompoundOrigins(
                        species_text=co["species_text"],
                        species_accession=co["species_accession"],
                        component_text=co["component_text"],
                        component_accession=co["component_accession"],
                        strain_text=co["strain_text"],
                        strain_accession=co["strain_accession"],
                        source_accession=co["source_accession"],
                        comments=co["comments"],
                    )
                    for co in df_compound_origins.to_dict(orient="records")
                ]
            }

            parents = {
                "parents": [
                    Relation(
                        id=int(parent.id.split(":")[1]),
                        chebi_accession=parent.id,
                        definition=parent.definition,
                        name=parent.name,
                    )
                    for parent in compound.superclasses().to_set()
                ]
            }

            children = {
                "children": [
                    Relation(
                        id=int(child.id.split(":")[1]),
                        chebi_accession=child.id,
                        definition=child.definition,
                        name=child.name,
                    )
                    for child in compound.subclasses().to_set()
                ]
            }

            relations = {relation: list(get_relations(compound, relation)) for relation in RelationsIds}

            data_suggestion = [{"input": s.name, "weight": 5} for s in synonyms["synonyms"] if s.scope == "related"] + [
                {"input": compound.name, "weight": 10 if initial_data["stars"] == 3 else 7}
            ]
            suggestions = {"suggestions": data_suggestion}

            compound_data = (
                initial_data
                | annotations
                | synonyms
                | structures
                | compound_origins
                | parents
                | children
                | relations
                | suggestions
            )
            record = Compound(**compound_data)
            record.meta.id = raw_id
            yield record.to_dict(include_meta=True)
        except KeyError as e:
            logging.warning(f"Error synchronizing compound {raw_id}: %s", e)
