from datetime import datetime, timedelta
from elasticsearch_dsl import AttrList, Boolean, Completion, Document, Float, InnerDoc, Integer, Keyword, Nested, Text
from enum import StrEnum, auto
from helper_functions.read_configuration import read_configuration
from indexation.settings_mappings import (
    alphanumeric_without_space_lowercase_normalizer,
    curie_first_part_normalizer,
    curie_second_part_normalizer,
    curie_alphanumeric_second_part_normalizer,
    english_greek_lowercase_analyzer,
    inchikey_first_part_normalizer,
    inchikey_second_part_normalizer,
    inchikey_lowercase_first_part_normalizer,
    inchikey_lowercase_second_part_normalizer,
    keyword_alphanumeric_without_space_lowercase_analyzer,
    keyword_alphanumeric_without_space_analyzer,
    keyword_greek_lowercase_analyzer,
    keyword_greek_analyzer,
    keyword_html_analyzer,
    keyword_lowercase_analyzer,
    lowercase_normalizer,
    small_english_greek_lowercase_analyzer,
)


class IndexAlias(StrEnum):
    chebi_dev_compounds = "chebi-dev-compounds"
    chebi_stage_compounds = "chebi-stage-compounds"
    chebi_prod_compounds = "chebi-prod-compounds"
    chebi_testing_compounds = "chebi-testing-compounds"  # This is an alias used by the unit tests of this repository.
    # This is an alias to create documents for chebi_for_testing (https://gitlab.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-ontology-generator/-/tree/dev/chebi_for_testing),
    # this index can be used by other projects to perform searches over its documents.
    chebi_subset_compounds = "chebi-subset-compounds"


class SynonymScope(StrEnum):
    EXACT = auto()
    RELATED = auto()


class RelationsIds(StrEnum):
    has_role = auto()
    has_part = auto()
    has_functional_parent = auto()
    has_parent_hydride = auto()
    is_conjugate_acid_of = auto()
    is_conjugate_base_of = auto()
    is_enantiomer_of = auto()
    is_substituent_group_from = auto()
    is_tautomer_of = auto()


class Synonym(InnerDoc):
    name = Text(
        fields={
            "lower": Text(analyzer=keyword_lowercase_analyzer),
            "greek": Text(analyzer=keyword_greek_analyzer),
            "html": Text(analyzer=keyword_html_analyzer),
            "lower_greek": Text(analyzer=keyword_greek_lowercase_analyzer),
            "special": Text(analyzer=keyword_alphanumeric_without_space_analyzer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
            "text": Text(analyzer=small_english_greek_lowercase_analyzer),
        }
    )
    database_reference = Keyword(
        fields={
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
        }
    )
    scope = Keyword()
    type = Keyword()


class CompoundOrigins(InnerDoc):
    species_text = Text(
        fields={
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
            "text": Text(analyzer=small_english_greek_lowercase_analyzer),
        }
    )
    species_accession = Keyword(
        fields={
            "raw_id": Keyword(normalizer=curie_second_part_normalizer),
            "special_raw_id": Keyword(normalizer=curie_alphanumeric_second_part_normalizer),
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
            "database_name": Keyword(normalizer=curie_first_part_normalizer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
        }
    )
    component_text = Text(
        fields={
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
            "text": Text(analyzer=small_english_greek_lowercase_analyzer),
        }
    )
    component_accession = Keyword(
        fields={
            # Suppose that component_accession = UBERON:0001359
            # - raw_id = 0001359
            # - lower = uberon:0001359
            # - special = uberon0001359
            # - identification = uberon0001359, but this would be used as the last option for a text search
            "raw_id": Keyword(normalizer=curie_second_part_normalizer),
            "special_raw_id": Keyword(normalizer=curie_alphanumeric_second_part_normalizer),
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
            "database_name": Keyword(normalizer=curie_first_part_normalizer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
        }
    )
    strain_text = Text(
        fields={
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
            "text": Text(analyzer=small_english_greek_lowercase_analyzer),
        }
    )
    strain_accession = Keyword(
        fields={
            "raw_id": Keyword(normalizer=curie_second_part_normalizer),
            "special_raw_id": Keyword(normalizer=curie_alphanumeric_second_part_normalizer),
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
            "database_name": Keyword(normalizer=curie_first_part_normalizer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
        }
    )
    source_accession = Keyword(
        fields={
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
        }
    )
    comments = Text(
        fields={
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
            "text": Text(analyzer=english_greek_lowercase_analyzer),
        }
    )


class Relation(InnerDoc):
    """Minimal compound has just a short set of information to describe an entity.
    It is used to model the relations, children and parents data"""

    chebi_accession = Keyword(
        fields={
            "raw_id": Keyword(normalizer=curie_second_part_normalizer),
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
        }
    )
    name = Text(
        fields={
            "lower": Text(analyzer=keyword_lowercase_analyzer),
            "greek": Text(analyzer=keyword_greek_analyzer),
            "lower_greek": Text(analyzer=keyword_greek_lowercase_analyzer),
            "special": Text(analyzer=keyword_alphanumeric_without_space_analyzer),
            "html": Text(analyzer=keyword_html_analyzer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
            "text": Text(analyzer=small_english_greek_lowercase_analyzer),
        }
    )
    definition = Text(
        fields={
            "text": Text(analyzer=english_greek_lowercase_analyzer),
        }
    )
    chemical_role = Boolean()
    biological_role = Boolean()
    application = Boolean()


class Compound(Document):
    chebi_accession = Keyword(
        fields={
            # Suppose that chebi_accession = CHEBI:15377
            # - lower = chebi:15377
            # - special = chebi15377
            # - identification = chebi15377, but this would be used as the last option for a text search
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
        }
    )
    ascii_name = Keyword()
    suggestions = Completion()
    name = Text(
        fields={
            "lower": Text(analyzer=keyword_lowercase_analyzer),
            "greek": Text(analyzer=keyword_greek_analyzer),
            "lower_greek": Text(analyzer=keyword_greek_lowercase_analyzer),
            "special": Text(analyzer=keyword_alphanumeric_without_space_analyzer),
            "html": Text(analyzer=keyword_html_analyzer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
            "text": Text(analyzer=small_english_greek_lowercase_analyzer),
        }
    )
    definition = Text(
        fields={
            "lower": Text(analyzer=keyword_lowercase_analyzer),
            "greek": Text(analyzer=keyword_greek_analyzer),
            "lower_greek": Text(analyzer=keyword_greek_lowercase_analyzer),
            "special": Text(analyzer=keyword_alphanumeric_without_space_analyzer),
            "html": Text(analyzer=keyword_html_analyzer),
            "text": Text(analyzer=english_greek_lowercase_analyzer),
        }
    )
    monoisotopicmass = Float()
    formula = Keyword(
        fields={
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
        }
    )
    mass = Float()
    charge = Integer()
    inchikey = Keyword(
        fields={
            # Suppose an inchikey like: RAXXELZNTBOGNW-UHFFFAOYSA-O
            # - special = raxxelzntbognw-uhfffaoysa-o
            # - first_part = RAXXELZNTBOGNW
            # - second_part = RAXXELZNTBOGNW-UHFFFAOYSA
            # - first_part_lower = raxxelzntbognw
            # - second_part_lower = raxxelzntbognw-uhfffaoysa
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
            "first_part": Keyword(normalizer=inchikey_first_part_normalizer),
            "second_part": Keyword(normalizer=inchikey_second_part_normalizer),
            "first_part_lower": Keyword(normalizer=inchikey_lowercase_first_part_normalizer),
            "second_part_lower": Keyword(normalizer=inchikey_lowercase_second_part_normalizer),
        }
    )
    inchi = Keyword()
    smiles = Keyword(
        fields={
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
        }
    )
    stars = Keyword(fields={"integer": Integer()})
    wurcs = Keyword()
    alternative_ids = Keyword(
        fields={
            # alternative_ids field is just a list of CHEBI ids, so we apply the same subfields as the chebi_accession field,
            # changing the prefix name
            "raw_id": Keyword(normalizer=curie_second_part_normalizer),
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
        }
    )
    database_references = Keyword(
        fields={
            # Suppose that a database_reference is 'CAS:7732-18-5'
            # raw_id = 7732-18-5
            # special_raw_id = 7732185
            # lower = cas:7732-18-5
            # special = cas7732185
            # database_name = cas
            # database_references_identification = cas7732185 it is used as a last option for search text matching
            "raw_id": Keyword(normalizer=curie_second_part_normalizer),
            "special_raw_id": Keyword(normalizer=curie_alphanumeric_second_part_normalizer),
            "lower": Keyword(normalizer=lowercase_normalizer),
            "special": Keyword(normalizer=alphanumeric_without_space_lowercase_normalizer),
            "database_name": Keyword(normalizer=curie_first_part_normalizer),
            "identification": Text(analyzer=keyword_alphanumeric_without_space_lowercase_analyzer),
        }
    )
    default_structure = Keyword()
    structures = AttrList([])
    synonyms = Nested(Synonym)
    compound_origins = Nested(CompoundOrigins)
    # Relations, take care the properties names, they should match with our RelationsIds Enum.
    parents = Nested(Relation)
    children = Nested(Relation)
    has_role = Nested(Relation)
    has_part = Nested(Relation)
    has_functional_parent = Nested(Relation)
    has_parent_hydride = Nested(Relation)
    is_conjugate_acid_of = Nested(Relation)
    is_conjugate_base_of = Nested(Relation)
    is_enantiomer_of = Nested(Relation)
    is_substituent_group_from = Nested(Relation)
    is_tautomer_of = Nested(Relation)

    class Index:
        settings = {
            "number_of_shards": 1,
            "number_of_replicas": 0,
            "max_result_window": 30_000,
            "mapping": {"nested_objects": {"limit": 500_000}},
        }

        @staticmethod
        def alias() -> str:
            """Getting the alias defined in the config.yml file"""
            configuration_elasticsearch = read_configuration().get("databases").get("elasticsearch")
            return configuration_elasticsearch.get("alias")

        @classmethod
        def pattern(cls) -> str:
            return f"{cls.alias()}-*"

        @classmethod
        def previous_index(cls) -> str:
            """Return the name of the previous index: If today is Dec 31 2023, then the previous index is
            chebi-dev-compounds-30122023 (Dec 30 2023).
            Note: we are supposing that alias is chebi-dev-compound for the above example."""
            return cls.pattern().replace("*", (datetime.now() - timedelta(days=1)).strftime("%d%m%Y"))

        @classmethod
        def next_index(cls) -> str:
            """Return the name of the next index to being created: If today is Dec 31 2023, then the next index is
            chebi-dev-compounds-01012024 (Jan 01 2024).
            Note: we are supposing that alias is chebi-dev-compound for the above example."""
            return cls.pattern().replace("*", datetime.now().strftime("%d%m%Y"))

        @classmethod
        @property
        def name(cls) -> str:
            """@property decorator is very important here. Reading the documentation, Index subclass must have a 'name'
            attribute to create the index using that name in ES (https://elasticsearch-dsl.readthedocs.io/en/latest/persistence.html#class-index-options).
            In our case, the 'name' attribute changes every day based on the current date, for that reason we can not
            just write the attribute on the top of the class, like settings attribute."""
            return cls.next_index()
