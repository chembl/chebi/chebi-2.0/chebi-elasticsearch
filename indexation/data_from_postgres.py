import pandas as pd
import numpy as np
import pandera as pa
import re

from helper_functions.database_connection import get_pg_connection
from helper_functions.sql_functions import get_sql_sentence
from pandera.typing import Series
from typing import NamedTuple, List


@pa.extensions.register_check_method
def valid_structures_regex(value: str):
    pattern = r"^\d+(;\d+)*$"
    return bool(re.match(pattern, value)) or value is None


class OutputBasicCompound(pa.DataFrameModel):
    compound_id: Series[int] = pa.Field(unique=True, nullable=False, coerce=True, title="compound id")
    ascii_name: Series[str] = pa.Field(nullable=False, unique=False, coerce=True, title="compound ascii name")
    modified_on: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="date of modification")


class InputStructure(pa.DataFrameModel):
    compound_id: Series[int] = pa.Field(unique=True, nullable=False, coerce=True, title="compound id")
    structures: Series[str] = pa.Field(
        unique=False, nullable=True, coerce=True, title="structures", valid_structures_regex=()
    )
    default_structure: Series[int] = pa.Field(nullable=True, coerce=True)


class OutputStructure(pa.DataFrameModel):
    compound_id: Series[int] = pa.Field(unique=True, nullable=False, coerce=True, title="compound id")
    structures: Series[List[int]] = pa.Field(unique=False, nullable=True, coerce=True, title="structures")
    default_structure: Series[int] = pa.Field(nullable=True, coerce=True)


class OutputCompoundOrigins(pa.DataFrameModel):
    compound_id: Series[int] = pa.Field(nullable=False, unique=False, coerce=True, title="compound id")
    species_text: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="species text")
    component_text: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="component text")
    strain_text: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="strain text")
    species_accession: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="species accession")
    component_accession: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="component accession")
    source_accession: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="source accession")
    strain_accession: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="strain accession")
    comments: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="comments")
    component_accession_url: Series[str] = pa.Field(
        nullable=True, unique=False, coerce=True, title="component accession url"
    )
    source_accession_url: Series[str] = pa.Field(nullable=True, unique=False, coerce=True, title="source accession url")
    species_accession_url: Series[str] = pa.Field(
        nullable=True, unique=False, coerce=True, title="species accession url"
    )


class CompoundInformationFromPostgreSQL(NamedTuple):
    basic_information: OutputBasicCompound
    compound_origins: OutputCompoundOrigins
    structures: OutputStructure


def get_data_from_postgres() -> CompoundInformationFromPostgreSQL:
    with get_pg_connection().connect() as connection:
        df_compound_origins = pd.read_sql(get_sql_sentence("compound_origins.sql"), connection)
        df_structures = pd.read_sql(get_sql_sentence("structures.sql"), connection)
        df_basic_compounds = pd.read_sql(get_sql_sentence("basic_compounds.sql"), connection)

        # Convert structures column to a list
        df_structures["structures"] = df_structures["structures"].str.split(";")

        # Convert NA, nan and NaT to None
        for df in [df_structures, df_basic_compounds, df_compound_origins]:
            df.replace({np.nan: None, pd.NA: None, pd.NaT: None}, inplace=True)

    return CompoundInformationFromPostgreSQL(
        OutputBasicCompound(df_basic_compounds),
        OutputCompoundOrigins(df_compound_origins),
        OutputStructure(df_structures),
    )
