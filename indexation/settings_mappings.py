from elasticsearch_dsl import analyzer, analysis, normalizer, tokenizer
from indexation.mappings import greek_letters_mappings, remove_characters_mapping
# Character filters

# 1. Delete whatever character different to numbers or letters
alphanumeric_with_space_filter = analysis.char_filter(
    "alphanumeric_with_space_filter", type="pattern_replace", pattern="[^a-zA-Z0-9\s]", replacement=""
)

# 2. curie_first_part_filter: This filter uses a regex to fetch the first part of a curie. Regex pattern matches
# one or more all alphanumeric characters and '-' '.' and '_' symbols, and does not match the word CHEBI in whichever
# combination (chEbI, ChEBI, etc.). Take a look the parenthesis, they are bordering the expression before the ':',
# which is the first part of the curie.
curie_first_part_filter = analysis.char_filter(
    "curie_first_part_filter",
    type="pattern_replace",
    pattern="^(?i)(?!.*chebi)([a-z-A-Z0-9\-\.\_]+)\:[a-z-A-Z0-9\-\.\_\(\),\%\/\#']+",
    replacement="$1",
)

# 3. curie_second_part_filter: Second part of a curie is always the identifier of a resource, we are going to take into
# account only alphanumeric and -, _ and . characters. Expression (?![A-Za-z]+$) is a "Negative Lookahead", which does
# not allow alphabetic expressions. For example: Wikipedia:water is not allowed, but Wikipedia:water123 or
# Wikipedia:123wat3r are allowed. Finally, the parenthesis are bordering the expression after the ':', which are the
# 2nd part of the curie.
curie_second_part_filter = analysis.char_filter(
    "curie_second_part_filter",
    type="pattern_replace",
    pattern="[a-z-A-Z0-9\-\.\_]+\:(?![A-Za-z]+$)([a-z-A-Z0-9\-\.\_\(\),\%\/\#']+)",
    replacement="$1",
)

# The InChIKey currently consists of three parts separated by hyphens, of 14, 10 and one character(s), respectively,
# like XXXXXXXXXXXXXX-YYYYYYYYFV-P. In this filter we are getting the first part, which is the first 14 character before
# the hyphen XXXXXXXXXXXXXX
inchikey_first_part_filter = analysis.char_filter(
    "inchikey_first_part_filter",
    type="pattern_replace",
    pattern="([a-z-A-z]{14})\-[a-z-A-z]{10}\-[a-z-A-z]",
    replacement="$1",
)

# In this filter we are getting the first and seconf part together, it means: XXXXXXXXXXXXXX-YYYYYYYYFV
inchikey_second_part_filter = analysis.char_filter(
    "inchikey_second_part_filter",
    type="pattern_replace",
    pattern="([a-z-A-z]{14}\-[a-z-A-z]{10})\-[a-z-A-z]",
    replacement="$1",
)

# Token filters

# 1. This token filter converts greek characters to the equivalent in English language, for example: α -> alpha
greek_synonyms_token_filter = analysis.token_filter(
    "greek_synonym_filter", type="synonym", synonyms_path="synonyms/greek_letters_synonyms.txt"
)

# Char Filters
greek_synonyms_char_filter = analysis.char_filter(
    "greek_synonym_char_filter", type="mapping", mappings=greek_letters_mappings
)
remove_special_characters_char_filter = analysis.char_filter(
    "remove_special_characters_char_filter", type="mapping", mappings=remove_characters_mapping
)

# 2. English language token filters are set as Elasticsearch propose here: https://www.elastic.co/guide/en/elasticsearch/reference/7.5/analysis-lang-analyzer.html#english-analyzer
english_stop = analysis.token_filter("english_stop", type="stop", stopwords="_english_")
english_stemmer = analysis.token_filter("english_stemmer", type="stemmer", language="english")
english_possessive_stemmer = analysis.token_filter(
    "english_possessive_stemmer", type="stemmer", language="possessive_english"
)

# 3. This token filter limits the number of generated tokens to 1000. Checking in the PostgreSQL database, the max
# length of definition compound and compound name are < 3000.

limit_length_filter = analysis.token_filter("limit_text", type="limit", tokenizer="standard", max_token_count=1_000)
small_limit_length_filter = analysis.token_filter(
    "small_limit_text", type="limit", tokenizer="standard", max_token_count=20
)

# Analyzers

english_greek_lowercase_analyzer = analyzer(
    "english_greek_lowercase_analyzer",
    type="custom",
    tokenizer=tokenizer(
        "custom_tokenizer", type="char_group", tokenize_on_chars=["whitespace", "—"]
    ),  # tokenizer by white space and special dash — (different to normal dash -)
    filter=[english_stop, english_stemmer, english_possessive_stemmer, limit_length_filter, "lowercase"],
    char_filter=["html_strip", greek_synonyms_char_filter, remove_special_characters_char_filter],
)

small_english_greek_lowercase_analyzer = analyzer(
    "small_english_greek_lowercase_analyzer",
    type="custom",
    tokenizer=tokenizer(
        "custom_tokenizer", type="char_group", tokenize_on_chars=["whitespace", "—"]
    ),  # tokenizer by white space and special dash — (different to normal dash -)
    filter=[english_stop, english_stemmer, english_possessive_stemmer, small_limit_length_filter, "lowercase"],
    char_filter=["html_strip", greek_synonyms_char_filter],
)

# keywords analyzers are necessary to transform a full token which is not a proper Keyword, but a Text field.
# For example, the chebi names.

keyword_lowercase_analyzer = analyzer(
    "keyword_lowercase_analyzer",
    type="custom",
    tokenizer="keyword",
    filter=["lowercase", "trim"],
    char_filter=["html_strip"],
)

keyword_html_analyzer = analyzer(
    "keyword_html_analyzer", type="custom", tokenizer="keyword", filter=["trim"], char_filter=["html_strip"]
)

# An analyser to remove special characters and replace them with '', it is used in the text fields
keyword_alphanumeric_without_space_lowercase_analyzer = analyzer(
    "keyword_alphanumeric_without_space_lowercase_analyzer",
    type="custom",
    tokenizer="keyword",
    filter=["lowercase", "trim"],
    char_filter=["html_strip", alphanumeric_with_space_filter],
)

keyword_alphanumeric_without_space_analyzer = analyzer(
    "keyword_alphanumeric_without_space_analyzer",
    type="custom",
    tokenizer="keyword",
    filter=["trim"],
    char_filter=["html_strip", alphanumeric_with_space_filter],
)

keyword_greek_lowercase_analyzer = analyzer(
    "keyword_greek_lowercase_analyzer",
    type="custom",
    tokenizer="keyword",
    filter=["lowercase", "trim"],
    char_filter=["html_strip", greek_synonyms_char_filter],
)

keyword_greek_analyzer = analyzer(
    "keyword_greek_analyzer",
    type="custom",
    tokenizer="keyword",
    filter=["trim"],
    char_filter=["html_strip", greek_synonyms_char_filter],
)

# Normalizers

# 1. Give the first part of a curie. What is a curie?: https://cthoyt.com/2021/09/14/curies.html
curie_first_part_normalizer = normalizer(
    "curie_first_part", type="custom", char_filter=[curie_first_part_filter], filter=["lowercase", "trim"]
)

# 2. Give the second part of a curie.
curie_second_part_normalizer = normalizer(
    "curie_second_part", type="custom", char_filter=[curie_second_part_filter], filter=["lowercase", "trim"]
)

# 3. Give the first part and the second part of the inchikey.
inchikey_first_part_normalizer = normalizer(
    "inchikey_first_part", type="custom", char_filter=[inchikey_first_part_filter]
)

inchikey_second_part_normalizer = normalizer(
    "inchikey_second_part", type="custom", char_filter=[inchikey_second_part_filter]
)

inchikey_lowercase_first_part_normalizer = normalizer(
    "inchikey_first_part", type="custom", char_filter=[inchikey_first_part_filter], filter=["lowercase", "trim"]
)

inchikey_lowercase_second_part_normalizer = normalizer(
    "inchikey_second_part", type="custom", char_filter=[inchikey_second_part_filter], filter=["lowercase", "trim"]
)

# 4. Let only alphanumeric characters in the second part of a curie. The second part of a curie is always a resource identifier
curie_alphanumeric_second_part_normalizer = normalizer(
    "curie_alphanumeric_second_part",
    type="custom",
    char_filter=[curie_second_part_filter, alphanumeric_with_space_filter],
    filter=["lowercase", "trim"],
)


# 5. Lowercase the entire token.
lowercase_normalizer = normalizer("lowercase", type="custom", filter=["lowercase", "trim"])

# 6. Remove special characters and spaces, letting alphanumeric characters, this is necessary to use it on the keywords
# fields, because a keyword field can not have an analyser.
alphanumeric_without_space_lowercase_normalizer = normalizer(
    "alphanumeric_without_space_lowercase_normalizer",
    type="custom",
    char_filter=[alphanumeric_with_space_filter],
    filter=["lowercase", "trim"],
)
