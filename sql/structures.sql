WITH cte_all_structures AS (
    SELECT
        c.id AS compound_id, --primary_id,
        STRING_AGG(DISTINCT s.id::varchar, ';') AS structures
    FROM chebi_allstar.compounds AS c
        LEFT JOIN chebi_allstar.compounds AS c2 ON c.id = c2.parent_id
        LEFT JOIN chebi_allstar.structures AS s ON
            c.id = s.compound_id OR c2.id = s.compound_id
    WHERE
        s.molfile IS NOT NULL
        AND c.parent_id IS NULL
    GROUP BY c.id
),

cte_default_structure AS (
    SELECT DISTINCT
        vs.compound_id,
        vs.structure_id AS default_structure
    FROM chebi_ontology.valid_structures AS vs
),

cte_structure AS (
    SELECT
        cas.compound_id,
        cas.structures,
        cds.default_structure
    FROM cte_all_structures AS cas
        INNER JOIN cte_default_structure AS cds ON cas.compound_id = cds.compound_id
)

SELECT * FROM cte_structure;
