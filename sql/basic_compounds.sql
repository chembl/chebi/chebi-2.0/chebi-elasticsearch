SELECT
    c.id AS compound_id,
    TO_CHAR(c.modified_on, 'DD Mon YYYY') AS modified_on,
    c.ascii_name
FROM chebi_ontology.compound_base_information AS c;
