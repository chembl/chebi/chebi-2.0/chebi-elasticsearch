WITH cte_compound_origins AS (
    SELECT
        vco.compound_id,
        vco.species_text,
        vco.species_accession,
        vco.component_text,
        vco.component_accession,
        vco.strain_text,
        -- TODO: Check this later, for now strain_accession is not necessary
        NULL AS strain_accession,
        vco.source_accession,
        vco.comments,
        CASE
            WHEN vco.component_accession IS NOT NULL THEN CONCAT('http://purl.obolibrary.org/obo/', REPLACE(vco.component_accession, ':', '_'))
        END AS component_accession_url,
        s.url AS source_accession_url,
        spe.url AS species_accession_url
    FROM chebi_ontology.valid_compound_origins AS vco
        LEFT JOIN chebi_allstar.source AS spe ON vco.species_source_id = spe.id
        LEFT JOIN chebi_allstar.source AS s ON vco.source_id = s.id
)

SELECT * FROM cte_compound_origins;
