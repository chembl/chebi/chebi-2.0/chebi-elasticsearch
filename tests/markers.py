import elasticsearch.exceptions
import pytest

from helper_functions.database_connection import get_elasticsearch_connection


def _require_es_connection():
    """This is a marker which skip a unit test to be executed if there is not a valid Elasticsearch connection set in
    the environment. For example:
        1. When you are running the test test_indexation_process on Gitlab Runner as part of the CI/CD
        2. When you are running the test test_indexation_process locally and the VPN is off"""
    try:
        get_elasticsearch_connection().info()
        is_valid_connection = True
    except elasticsearch.exceptions.ConnectionError:
        is_valid_connection = False

    return pytest.mark.skipif(
        not is_valid_connection, reason="This unit tests requires a valid Elasticsearch connection"
    )


require_es_connection = _require_es_connection()
