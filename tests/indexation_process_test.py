import json

import pytest
import psycopg2

from datetime import datetime, timedelta
from enum import IntEnum

from indexation.data_from_postgres import get_data_from_postgres
from indexation.documents import get_compounds, RelationsIds
from indexation.entities import Compound, IndexAlias
from indexation_process import main
from io import StringIO
from operator import itemgetter
from tests.markers import require_es_connection
from unittest.mock import patch


class CountCompounds(IntEnum):
    # The testing ontology has 15 entries (12 compounds and 3 roles definitions), once the ontology has been
    # converted to ES documents, we will have 12 entries (9 compounds and 3 roles definitions),
    # because there are 2 deprecated compounds and one 1_STAR compound
    TOTAL_TERMS = 15
    TOTAL_TERMS_TO_INDEX = 12


def sort_results(data: list[dict]) -> list[dict]:
    """
    This is an auxiliary function which sorted the Elasticsearch documents (which are python dicts). It is necesary because
    we have lists inside our documents, those list have to be sorted under some criteria always. The above allows to compare
    the expected elastic search results against the generated ones
    :param data: dictionary as an elastic search document
    :return: sorted elastic search document
    """
    records = list(data)
    for record in records:
        # We'll have always 'parents' and 'children' fields in our results. In the basic case, a compound is parent and
        # child from itself.
        record["_source"]["parents"] = sorted(record["_source"]["parents"], key=itemgetter("id"))
        record["_source"]["children"] = sorted(record["_source"]["children"], key=itemgetter("id"))
        record["_source"]["suggestions"] = sorted(record["_source"]["suggestions"], key=itemgetter("input"))

        # Sort relations by id
        if "relations" in record["_source"]:
            record["_source"]["relations"] = sorted(record["_source"]["relations"], key=itemgetter("id"))
        # Sort 'compound_origins' by 'species_accession' and 'component_accession'
        if "compound_origins" in record["_source"]:
            record["_source"]["compound_origins"] = sorted(
                record["_source"]["compound_origins"],
                key=lambda s: (s.get("species_accession"), s.get("component_accession", "")),
            )
        # Sort 'synonyms' by 'name' and 'database_reference'
        if "synonyms" in record["_source"]:
            record["_source"]["synonyms"] = sorted(
                record["_source"]["synonyms"], key=lambda s: (s.get("name"), s.get("database_reference"))
            )
        # Sort 'alternative_ids' and 'database_references' by string (normal python sort function)
        if "alternative_ids" in record["_source"]:
            record["_source"]["alternative_ids"] = sorted(record["_source"]["alternative_ids"])
        if "database_references" in record["_source"]:
            record["_source"]["database_references"] = sorted(record["_source"]["database_references"])
        if "structures" in record["_source"]:
            record["_source"]["structures"] = sorted(record["_source"]["structures"])

    # Finally, sort the elastic search results by _id
    return sorted(records, key=itemgetter("_id"))


def test_get_compounds(
    chebi_fake,
    compounds_deprecated,
    compounds_1_star,
    compounds_2_star,
    compounds_3_star,
    compound_information_from_postgresql,
    expected_elasticsearch_records,
    index,
    caplog,
):
    """
    Test the ontology mapping process, it is used a sample of the real chebi ontology and based on that, it is generated
    the equivalents elasticsearch documents
    """
    with (
        patch("indexation.data_from_postgres.get_pg_connection"),
        patch("indexation.data_from_postgres.get_sql_sentence"),
        patch("indexation.data_from_postgres.pd.read_sql") as read_sql_mock,
    ):
        read_sql_mock.side_effect = compound_information_from_postgresql
        compound_info = get_data_from_postgres()
        # Here we get the list of documents which will be inserted in ES, documents are constructed using the internal
        # ElasticSearch DSL classes, for example:
        # [... {{'id': 22712, 'chebi_accession': 'CHEBI:22712', 'definition': Definition('Any benzenoid aromatic..')}] ,
        # therefore, in order to get the raw data, we need to serialize the information. For the above is used StringIO
        compounds_records = list(get_compounds(compound_info))
        # We set the _index property to our index name used in the unit tests
        for record in compounds_records:
            record["_index"] = index._name
        in_memory_file = StringIO()
        json.dump(compounds_records, in_memory_file)
        # Here, we have the data already serialized it do not have ElasticSearch DSL internal classes anymore
        raw_compound_records = in_memory_file.getvalue()
        # Now, convert the serialized data in a python dictionary and, we sort it
        records = sort_results(json.loads(raw_compound_records))
    # sort the expected results, so at this point we have both: expected and generated data already sorted!
    expected_elasticsearch_records = sort_results(expected_elasticsearch_records)

    chebi_accessions = {c["_source"]["chebi_accession"] for c in records}

    # No 1_STAR compounds nor deprecated compounds should be included in the results.
    assert all(_id not in compounds_1_star.ids | compounds_deprecated.ids for _id in chebi_accessions)
    # 2_STAR and 3_STAR compounds should be in the results
    assert all(_id in compounds_3_star.ids | compounds_2_star.ids for _id in chebi_accessions)

    assert len(chebi_fake.terms()) == CountCompounds.TOTAL_TERMS
    assert len(chebi_accessions) == len(expected_elasticsearch_records) == CountCompounds.TOTAL_TERMS_TO_INDEX

    # All compound should have parents and children, because in the most basic case a compound is parent and child
    # from itself. On the other hand, a compound could not have other kind of relations.
    assert all("parents" in compound["_source"] for compound in records)
    assert all("children" in compound["_source"] for compound in records)
    assert any(relation in compound["_source"] for compound in records for relation in RelationsIds)

    # Compare all the inner documents and inner lists in both results: generated and expected
    for record, expected_record in zip(records, expected_elasticsearch_records, strict=True):
        assert record["_source"]["parents"] == expected_record["_source"]["parents"]
        assert record["_source"]["children"] == expected_record["_source"]["children"]
        assert record["_source"].get("has_role", None) == expected_record["_source"].get("has_role", None)
        assert record["_source"].get("synonyms", None) == expected_record["_source"].get("synonyms", None)
        assert record["_source"].get("alternative_ids", None) == expected_record["_source"].get("alternative_ids", None)
        assert record["_source"].get("database_references", None) == expected_record["_source"].get(
            "database_references", None
        )

    # We compare whole documents, they should have the same information
    assert records == expected_elasticsearch_records

    # Finally, check logs for some errors
    for logs in caplog.records:
        assert logs.levelname != "ERROR"


@pytest.mark.parametrize(
    "current_date, my_alias",
    [
        (datetime(2023, 6, 20, 10, 50), "chebi-dev-compounds"),
        (datetime(2021, 3, 10, 23, 50), "chebi-stage-compounds"),
        (datetime.now(), "chebi-prod-compounds"),
    ],
)
def test_indexes_names(current_date, my_alias, mocker, caplog):
    """
    Test the correct generation of the elasticsearch index names
    """

    mock_date = mocker.patch("indexation.entities.datetime")
    mock_date.now.return_value = current_date
    with patch(
        "indexation.entities.read_configuration", return_value={"databases": {"elasticsearch": {"alias": my_alias}}}
    ):
        previous_index = f'{my_alias}-{(current_date - timedelta(days=1)).strftime("%d%m%Y")}'
        next_index = f'{my_alias}-{current_date.strftime("%d%m%Y")}'

        assert Compound.Index.previous_index() == previous_index
        assert Compound.Index.next_index() == Compound.Index.name == next_index

        # Finally, check logs for some errors
        for logs in caplog.records:
            assert logs.levelname != "ERROR"


def test_fail_indexation_alias(compound_information_from_postgresql, mocker):
    """Testing an error in the allowed aliases names"""

    mock_date = mocker.patch("indexation.entities.datetime")
    mock_date.now.return_value = datetime.now()
    test_alias = "chebi-fail-compounds"

    with (
        patch("indexation.data_from_postgres.get_pg_connection"),
        patch("indexation_process.Compound.Index.alias") as alias,
    ):
        alias.side_effect = lambda: test_alias
        with pytest.raises(KeyError, match=f"Alias {test_alias} is not allowed"):
            main()


@require_es_connection
def test_fail_indexation_postgres_connection(mocker):
    """Testing an error in the postgresql database"""

    mock_date = mocker.patch("indexation.entities.datetime")
    mock_date.now.return_value = datetime.now()

    # Use our chebi alias for testing, we do not want to touch the important aliases: chebi-dev-compounds,
    # chebi-stage-compounds and chebi-prod-compounds
    test_alias = IndexAlias.chebi_testing_compounds.value
    with (
        patch("indexation.data_from_postgres.get_pg_connection") as pg_connection,
        patch("indexation_process.Compound.Index.alias") as alias,
    ):
        alias.side_effect = lambda: test_alias
        pg_connection.side_effect = psycopg2.OperationalError
        with pytest.raises(psycopg2.OperationalError):
            main()


@require_es_connection
def test_indexation_process(
    compound_information_from_postgresql, expected_elasticsearch_records, mocker, chebi_fake, index, caplog
):
    """
    Test the entire indexation process checking the full flow. It is necessary an Elasticsearch connection because it will
    create the index in ES.
    Note: This test will be skipped if an Elasticsearch is not set properly to avoid errors
    """
    mock_date = mocker.patch("indexation.entities.datetime")
    mock_date.now.return_value = datetime.now()

    # Use our chebi alias for testing, we do not want to touch the important aliases: chebi-dev-compounds,
    # chebi-stage-compounds and chebi-prod-compounds
    test_alias = IndexAlias.chebi_testing_compounds.value
    with (
        patch("indexation.data_from_postgres.get_pg_connection"),
        patch("indexation.data_from_postgres.pd.read_sql") as read_sql_mock,
        patch("indexation_process.Compound.Index.alias") as alias,
        patch("indexation_process.get_compounds") as get_compounds_mock,
    ):
        read_sql_mock.side_effect = compound_information_from_postgresql
        get_compounds_mock.return_value = expected_elasticsearch_records
        alias.side_effect = lambda: test_alias
        main()

        # Finally, check logs for some errors
        for logs in caplog.records:
            assert logs.levelname != "ERROR"

        assert len(chebi_fake.terms()) == 15
        assert f"Index {index._name} created" in caplog.text
        assert f"Has been created {len(expected_elasticsearch_records)} documents in {index._name} index" in caplog.text
        assert f"Alias {test_alias} assigned to {index._name} correctly" in caplog.text
        assert "ChEBI Indexation process finished successfully" in caplog.text
