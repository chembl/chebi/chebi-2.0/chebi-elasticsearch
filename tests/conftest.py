import pandas as pd
import pronto as pt
import pytest

from datetime import datetime
from collections.abc import Iterable
from elasticsearch_dsl import Index
from helper_functions.database_connection import get_elasticsearch_connection
from pathlib import Path

from indexation.entities import IndexAlias

current_folder = Path(__file__).parent.absolute().as_posix()
root_folder = Path(current_folder)
index_name = f'{IndexAlias.chebi_testing_compounds.value}-{datetime.now().strftime("%d%m%Y")}'


@pytest.fixture(scope="module")
def chebi_fake() -> pt.Ontology:
    return pt.Ontology(root_folder / "mock_files" / "chebi.owl.gz")


@pytest.fixture(scope="module")
def compounds_deprecated(chebi_fake) -> pt.TermSet:
    return pt.TermSet(c for c in chebi_fake.terms() if c.obsolete)


@pytest.fixture(scope="module")
def index() -> Index:
    return Index(index_name, using=get_elasticsearch_connection())


@pytest.fixture(scope="module")
def compounds_1_star(chebi_fake) -> pt.TermSet:
    return pt.TermSet(c for c in chebi_fake.terms() if "chebi/1:STAR" in c.subsets)


@pytest.fixture(scope="module")
def compounds_2_star(chebi_fake) -> pt.TermSet:
    return pt.TermSet(c for c in chebi_fake.terms() if "chebi/2:STAR" in c.subsets)


@pytest.fixture(scope="module")
def compounds_3_star(chebi_fake) -> pt.TermSet:
    return pt.TermSet(c for c in chebi_fake.terms() if "chebi/3:STAR" in c.subsets)


@pytest.fixture(scope="module")
def compound_information_from_postgresql() -> Iterable:
    """Generate fake data for 'CHEBI_1', 'CHEBI_1000', 'CHEBI_129091','CHEBI_129151','CHEBI_22712','CHEBI_33836','CHEBI_36820',
    'CHEBI_38777','CHEBI_15377','CHEBI_76413','CHEBI_16234', 'CHEBI_29412', 'CHEBI_76413', 'CHEBI_51086', 'CHEBI_33232', 'CHEBI:1230'.
     Same compounds than tests/chebi.owl"""

    return (
        pd.read_csv(root_folder / "mock_files" / "compound_origins.tsv", sep="\t"),
        pd.read_csv(root_folder / "mock_files" / "structures.tsv", sep="\t"),
        pd.read_csv(root_folder / "mock_files" / "basic_compounds.tsv", sep="\t"),
    )


@pytest.fixture(scope="module")
def expected_elasticsearch_records(index) -> list[dict]:
    return [
        {
            "_id": 129151,
            "_index": f"{index._name}",
            "_source": {
                "chebi_accession": "CHEBI:129151",
                "name": "(6S,7S,8R)-7-(4-bromophenyl)-8-(hydroxymethyl)-1,4-diazabicyclo[4.2.0]octan-2-one",
                "ascii_name": "(6S,7S,8R)-7-(4-bromophenyl)-8-(hydroxymethyl)-1,4-diazabicyclo[4.2.0]octan-2-one",
                "stars": 2,
                "default_structure": 3054419,
                "database_references": ["LINCS:LSM-40702"],
                "smiles": "O=C1CNC[C@@H]2[C@H](c3ccc(Br)cc3)[C@H](CO)N12",
                "inchikey": "CPZPDKPINUWRBY-MDZLAQPJSA-N",
                "inchi": "InChI=1S/C13H15BrN2O2/c14-9-3-1-8(2-4-9)13-10-5-15-6-12(18)16(10)11(13)7-17/h1-4,10-11,13,15,17H,5-7H2/t10-,11+,13+/m1/s1",
                "structures": [3054419],
                "parents": [
                    {
                        "id": 129151,
                        "chebi_accession": "CHEBI:129151",
                        "name": "(6S,7S,8R)-7-(4-bromophenyl)-8-(hydroxymethyl)-1,4-diazabicyclo[4.2.0]octan-2-one",
                    },
                    {
                        "id": 36820,
                        "chebi_accession": "CHEBI:36820",
                        "definition": "Two or more cyclic systems (single rings or fused systems) which are directly joined to each other by double or single bonds are named ring assemblies when the number of such direct ring junctions is one less than the number of cyclic systems involved.",
                        "name": "ring assembly",
                    },
                    {"id": 38777, "chebi_accession": "CHEBI:38777", "name": "azetidines"},
                    {
                        "id": 22712,
                        "chebi_accession": "CHEBI:22712",
                        "definition": "Any benzenoid aromatic compound consisting of the benzene skeleton and its substituted derivatives.",
                        "name": "benzenes",
                    },
                    {"id": 33836, "chebi_accession": "CHEBI:33836", "name": "benzenoid aromatic compound"},
                ],
                "children": [
                    {
                        "id": 129151,
                        "chebi_accession": "CHEBI:129151",
                        "name": "(6S,7S,8R)-7-(4-bromophenyl)-8-(hydroxymethyl)-1,4-diazabicyclo[4.2.0]octan-2-one",
                    }
                ],
                "suggestions": [
                    {
                        "input": "(6S,7S,8R)-7-(4-bromophenyl)-8-(hydroxymethyl)-1,4-diazabicyclo[4.2.0]octan-2-one",
                        "weight": 7,
                    }
                ],
            },
        },
        {
            "_id": 22712,
            "_index": f"{index._name}",
            "_source": {
                "chebi_accession": "CHEBI:22712",
                "name": "benzenes",
                "ascii_name": "benzenes",
                "stars": 3,
                "definition": "Any benzenoid aromatic compound consisting of the benzene skeleton and its substituted derivatives.",
                "modified_on": "21 Feb 2022",
                "parents": [
                    {
                        "id": 22712,
                        "chebi_accession": "CHEBI:22712",
                        "definition": "Any benzenoid aromatic compound consisting of the benzene skeleton and its substituted derivatives.",
                        "name": "benzenes",
                    },
                    {"id": 33836, "chebi_accession": "CHEBI:33836", "name": "benzenoid aromatic compound"},
                ],
                "children": [
                    {
                        "id": 22712,
                        "chebi_accession": "CHEBI:22712",
                        "definition": "Any benzenoid aromatic compound consisting of the benzene skeleton and its substituted derivatives.",
                        "name": "benzenes",
                    },
                    {
                        "id": 129151,
                        "chebi_accession": "CHEBI:129151",
                        "name": "(6S,7S,8R)-7-(4-bromophenyl)-8-(hydroxymethyl)-1,4-diazabicyclo[4.2.0]octan-2-one",
                    },
                ],
                "suggestions": [{"input": "benzenes", "weight": 10}],
            },
        },
        {
            "_id": 33836,
            "_index": f"{index._name}",
            "_source": {
                "chebi_accession": "CHEBI:33836",
                "name": "benzenoid aromatic compound",
                "ascii_name": "benzenoid aromatic compound",
                "stars": 3,
                "modified_on": "15 Mar 2018",
                "synonyms": [
                    {"name": "benzenoid aromatic compounds", "scope": "related", "database_reference": "ChEBI"},
                    {"name": "benzenoid compound", "scope": "related", "database_reference": "ChEBI"},
                ],
                "parents": [{"id": 33836, "chebi_accession": "CHEBI:33836", "name": "benzenoid aromatic compound"}],
                "children": [
                    {
                        "id": 22712,
                        "chebi_accession": "CHEBI:22712",
                        "definition": "Any benzenoid aromatic compound consisting of the benzene skeleton and its substituted derivatives.",
                        "name": "benzenes",
                    },
                    {
                        "id": 129151,
                        "chebi_accession": "CHEBI:129151",
                        "name": "(6S,7S,8R)-7-(4-bromophenyl)-8-(hydroxymethyl)-1,4-diazabicyclo[4.2.0]octan-2-one",
                    },
                    {"id": 33836, "chebi_accession": "CHEBI:33836", "name": "benzenoid aromatic compound"},
                ],
                "suggestions": [
                    {"input": "benzenoid aromatic compounds", "weight": 5},
                    {"input": "benzenoid compound", "weight": 5},
                    {"input": "benzenoid aromatic compound", "weight": 10},
                ],
            },
        },
        {
            "_id": 36820,
            "_index": f"{index._name}",
            "_source": {
                "chebi_accession": "CHEBI:36820",
                "name": "ring assembly",
                "ascii_name": "ring assembly",
                "stars": 3,
                "definition": "Two or more cyclic systems (single rings or fused systems) which are directly joined to each other by double or single bonds are named ring assemblies when the number of such direct ring junctions is one less than the number of cyclic systems involved.",
                "modified_on": "12 Mar 2021",
                "synonyms": [
                    {"name": "ring assembly", "scope": "exact", "database_reference": "IUPAC", "type": "IUPAC_NAME"},
                    {"name": "ring assemblies", "scope": "exact", "database_reference": "IUPAC", "type": "IUPAC_NAME"},
                ],
                "parents": [
                    {
                        "id": 36820,
                        "chebi_accession": "CHEBI:36820",
                        "definition": "Two or more cyclic systems (single rings or fused systems) which are directly joined to each other by double or single bonds are named ring assemblies when the number of such direct ring junctions is one less than the number of cyclic systems involved.",
                        "name": "ring assembly",
                    }
                ],
                "children": [
                    {
                        "id": 36820,
                        "chebi_accession": "CHEBI:36820",
                        "definition": "Two or more cyclic systems (single rings or fused systems) which are directly joined to each other by double or single bonds are named ring assemblies when the number of such direct ring junctions is one less than the number of cyclic systems involved.",
                        "name": "ring assembly",
                    },
                    {
                        "id": 129151,
                        "chebi_accession": "CHEBI:129151",
                        "name": "(6S,7S,8R)-7-(4-bromophenyl)-8-(hydroxymethyl)-1,4-diazabicyclo[4.2.0]octan-2-one",
                    },
                ],
                "suggestions": [{"input": "ring assembly", "weight": 10}],
            },
        },
        {
            "_id": 38777,
            "_index": f"{index._name}",
            "_source": {
                "chebi_accession": "CHEBI:38777",
                "name": "azetidines",
                "ascii_name": "azetidines",
                "stars": 3,
                "modified_on": "30 Nov 2021",
                "parents": [{"id": 38777, "chebi_accession": "CHEBI:38777", "name": "azetidines"}],
                "children": [
                    {
                        "id": 129151,
                        "chebi_accession": "CHEBI:129151",
                        "name": "(6S,7S,8R)-7-(4-bromophenyl)-8-(hydroxymethyl)-1,4-diazabicyclo[4.2.0]octan-2-one",
                    },
                    {"id": 38777, "chebi_accession": "CHEBI:38777", "name": "azetidines"},
                ],
                "suggestions": [{"input": "azetidines", "weight": 10}],
            },
        },
        {
            "_index": f"{index._name}",
            "_id": 51086,
            "_source": {
                "chebi_accession": "CHEBI:51086",
                "name": "chemical role",
                "ascii_name": "chemical role",
                "stars": 3,
                "definition": "A role played by the molecular entity or part thereof within a chemical context.",
                "modified_on": "20 Feb 2019",
                "parents": [
                    {
                        "id": 51086,
                        "chebi_accession": "CHEBI:51086",
                        "definition": "A role played by the molecular entity or part thereof within a chemical context.",
                        "name": "chemical role",
                    }
                ],
                "children": [
                    {
                        "id": 51086,
                        "chebi_accession": "CHEBI:51086",
                        "definition": "A role played by the molecular entity or part thereof within a chemical context.",
                        "name": "chemical role",
                    },
                    {
                        "id": 76413,
                        "chebi_accession": "CHEBI:76413",
                        "definition": "A gas in an atmosphere that absorbs and emits radiation within the thermal infrared range, so contributing to the 'greenhouse effect'.",
                        "name": "greenhouse gas",
                    },
                ],
                "suggestions": [{"input": "chemical role", "weight": 10}],
            },
        },
        {
            "_index": f"{index._name}",
            "_id": 33232,
            "_source": {
                "chebi_accession": "CHEBI:33232",
                "name": "application",
                "ascii_name": "application",
                "stars": 3,
                "definition": "Intended use of the molecular entity or part thereof by humans.",
                "modified_on": "08 Oct 2019",
                "parents": [
                    {
                        "id": 33232,
                        "chebi_accession": "CHEBI:33232",
                        "definition": "Intended use of the molecular entity or part thereof by humans.",
                        "name": "application",
                    }
                ],
                "children": [
                    {
                        "id": 33232,
                        "chebi_accession": "CHEBI:33232",
                        "definition": "Intended use of the molecular entity or part thereof by humans.",
                        "name": "application",
                    },
                    {
                        "id": 76413,
                        "chebi_accession": "CHEBI:76413",
                        "definition": "A gas in an atmosphere that absorbs and emits radiation within the thermal infrared range, so contributing to the 'greenhouse effect'.",
                        "name": "greenhouse gas",
                    },
                ],
                "suggestions": [{"input": "application", "weight": 10}],
            },
        },
        {
            "_index": f"{index._name}",
            "_id": 76413,
            "_source": {
                "chebi_accession": "CHEBI:76413",
                "name": "greenhouse gas",
                "ascii_name": "greenhouse gas",
                "stars": 3,
                "definition": "A gas in an atmosphere that absorbs and emits radiation within the thermal infrared range, so contributing to the 'greenhouse effect'.",
                "database_references": ["Wikipedia:Greenhouse_gas"],
                "modified_on": "16 Sep 2019",
                "synonyms": [{"name": "greenhouse gases", "scope": "related", "database_reference": "ChEBI"}],
                "parents": [
                    {
                        "id": 33232,
                        "chebi_accession": "CHEBI:33232",
                        "definition": "Intended use of the molecular entity or part thereof by humans.",
                        "name": "application",
                    },
                    {
                        "id": 51086,
                        "chebi_accession": "CHEBI:51086",
                        "definition": "A role played by the molecular entity or part thereof within a chemical context.",
                        "name": "chemical role",
                    },
                    {
                        "id": 76413,
                        "chebi_accession": "CHEBI:76413",
                        "definition": "A gas in an atmosphere that absorbs and emits radiation within the thermal infrared range, so contributing to the 'greenhouse effect'.",
                        "name": "greenhouse gas",
                    },
                ],
                "children": [
                    {
                        "id": 76413,
                        "chebi_accession": "CHEBI:76413",
                        "definition": "A gas in an atmosphere that absorbs and emits radiation within the thermal infrared range, so contributing to the 'greenhouse effect'.",
                        "name": "greenhouse gas",
                    }
                ],
                "suggestions": [{"input": "greenhouse gases", "weight": 5}, {"input": "greenhouse gas", "weight": 10}],
            },
        },
        {
            "_index": f"{index._name}",
            "_id": 15377,
            "_source": {
                "chebi_accession": "CHEBI:15377",
                "name": "water",
                "ascii_name": "water",
                "alternative_ids": [
                    "CHEBI:43228",
                    "CHEBI:13352",
                    "CHEBI:42857",
                    "CHEBI:5585",
                    "CHEBI:44292",
                    "CHEBI:27313",
                    "CHEBI:10743",
                    "CHEBI:44701",
                    "CHEBI:44819",
                    "CHEBI:42043",
                ],
                "stars": 3,
                "default_structure": 2018,
                "definition": "An oxygen hydride consisting of an oxygen atom that is covalently bonded to two hydrogen atoms",
                "database_references": [
                    "MolBase:1",
                    "KEGG:D00001",
                    "CAS:7732-18-5",
                    "PDBeChem:HOH",
                    "Gmelin:117",
                    "Wikipedia:Water",
                    "Beilstein:3587155",
                    "KEGG:C00001",
                    "MetaCyc:WATER",
                    "Reaxys:3587155",
                    "HMDB:HMDB0002111",
                ],
                "modified_on": "20 Nov 2019",
                "inchi": "InChI=1S/H2O/h1H2",
                "charge": 0,
                "mass": 15.999,
                "smiles": "[H]O[H]",
                "formula": "H2O",
                "inchikey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                "monoisotopicmass": 15.99491,
                "synonyms": [
                    {"name": "dihydridooxygen", "scope": "related", "database_reference": "IUPAC"},
                    {"name": "WATER", "scope": "exact", "database_reference": "PDBeChem"},
                    {"name": "oxidane", "scope": "exact", "database_reference": "IUPAC", "type": "IUPAC_NAME"},
                    {"name": "agua", "scope": "related", "database_reference": "ChEBI"},
                    {"name": "[OH2]", "scope": "related", "database_reference": "IUPAC"},
                    {"name": "HOH", "scope": "related", "database_reference": "ChEBI"},
                    {"name": "BOUND WATER", "scope": "related", "database_reference": "PDBeChem"},
                    {"name": "H2O", "scope": "related", "database_reference": "KEGG_COMPOUND"},
                    {"name": "H2O", "scope": "related", "database_reference": "UniProt"},
                    {"name": "aqua", "scope": "related", "database_reference": "ChEBI"},
                    {"name": "eau", "scope": "related", "database_reference": "ChEBI"},
                    {"name": "Water", "scope": "exact", "database_reference": "KEGG_COMPOUND"},
                    {"name": "hydrogen hydroxide", "scope": "related", "database_reference": "ChEBI"},
                    {"name": "acqua", "scope": "related", "database_reference": "ChEBI"},
                    {"name": "water", "scope": "exact", "database_reference": "IUPAC", "type": "IUPAC_NAME"},
                    {"name": "dihydrogen oxide", "scope": "related", "database_reference": "IUPAC"},
                    {"name": "Wasser", "scope": "related", "database_reference": "ChEBI"},
                ],
                "structures": [102, 2018],
                "compound_origins": [
                    {
                        "species_text": "Escherichia coli",
                        "species_accession": "562",
                        "source_accession": "21988831",
                    },
                    {
                        "species_text": "Homo sapiens",
                        "species_accession": "9606",
                        "source_accession": "10.1038/nbt.2488",
                    },
                    {
                        "species_text": "Mus musculus",
                        "species_accession": "10090",
                        "source_accession": "19425150",
                        "comments": "Source: BioModels - MODEL1507180067",
                    },
                    {
                        "species_text": "Saccharomyces cerevisiae",
                        "species_accession": "4932",
                        "source_accession": "24678285",
                        "comments": "Source: yeast.sf.net",
                    },
                ],
                "parents": [
                    {
                        "id": 15377,
                        "chebi_accession": "CHEBI:15377",
                        "definition": "An oxygen hydride consisting of an oxygen atom that is covalently bonded to two hydrogen atoms",
                        "name": "water",
                    }
                ],
                "children": [
                    {
                        "id": 15377,
                        "chebi_accession": "CHEBI:15377",
                        "definition": "An oxygen hydride consisting of an oxygen atom that is covalently bonded to two hydrogen atoms",
                        "name": "water",
                    }
                ],
                "has_role": [
                    {
                        "id": 76413,
                        "chebi_accession": "CHEBI:76413",
                        "definition": "A gas in an atmosphere that absorbs and emits radiation within the thermal infrared range, so contributing to the 'greenhouse effect'.",
                        "name": "greenhouse gas",
                        "chemical_role": True,
                        "biological_role": False,
                        "application": True,
                    }
                ],
                "is_conjugate_acid_of": [{"id": 16234, "chebi_accession": "CHEBI:16234", "name": "hydroxide"}],
                "is_conjugate_base_of": [{"id": 29412, "chebi_accession": "CHEBI:29412", "name": "oxonium"}],
                "suggestions": [
                    {"input": "BOUND WATER", "weight": 5},
                    {"input": "eau", "weight": 5},
                    {"input": "H2O", "weight": 5},
                    {"input": "H2O", "weight": 5},
                    {"input": "dihydridooxygen", "weight": 5},
                    {"input": "aqua", "weight": 5},
                    {"input": "acqua", "weight": 5},
                    {"input": "hydrogen hydroxide", "weight": 5},
                    {"input": "HOH", "weight": 5},
                    {"input": "[OH2]", "weight": 5},
                    {"input": "agua", "weight": 5},
                    {"input": "dihydrogen oxide", "weight": 5},
                    {"input": "Wasser", "weight": 5},
                    {"input": "water", "weight": 10},
                ],
            },
        },
        {
            "_index": f"{index._name}",
            "_id": 16234,
            "_source": {
                "chebi_accession": "CHEBI:16234",
                "name": "hydroxide",
                "ascii_name": "hydroxide",
                "alternative_ids": ["CHEBI:13419", "CHEBI:13365", "CHEBI:44641", "CHEBI:5594"],
                "stars": 3,
                "default_structure": 22582,
                "database_references": ["CAS:14280-30-9", "KEGG:C01328", "PDBeChem:OH", "Gmelin:24714"],
                "modified_on": "12 Dec 2018",
                "smiles": "[H][O-]",
                "charge": -1,
                "inchikey": "XLYOFNOQVPJJNP-UHFFFAOYSA-M",
                "monoisotopicmass": 15.99546,
                "mass": 15.999,
                "formula": "HO",
                "inchi": "InChI=1S/H2O/h1H2/p-1",
                "synonyms": [
                    {"name": "OH−", "scope": "related", "database_reference": "IUPAC"},
                    {"name": "HO-", "scope": "related", "database_reference": "KEGG_COMPOUND"},
                    {"name": "oxidanide", "scope": "exact", "database_reference": "IUPAC", "type": "IUPAC_NAME"},
                    {
                        "name": "hydridooxygenate(1−)",
                        "scope": "exact",
                        "database_reference": "IUPAC",
                        "type": "IUPAC_NAME",
                    },
                    {"name": "hydroxide", "scope": "exact", "database_reference": "IUPAC", "type": "IUPAC_NAME"},
                    {"name": "Hydroxide ion", "scope": "related", "database_reference": "KEGG_COMPOUND"},
                    {"name": "OH-", "scope": "related", "database_reference": "KEGG_COMPOUND"},
                    {"name": "HYDROXIDE ION", "scope": "related", "database_reference": "PDBeChem"},
                ],
                "structures": [2304, 22582],
                "compound_origins": [
                    {
                        "species_text": "Mus musculus",
                        "species_accession": "10090",
                        "source_accession": "19425150",
                        "comments": "Source: BioModels - MODEL1507180067",
                    }
                ],
                "parents": [{"id": 16234, "chebi_accession": "CHEBI:16234", "name": "hydroxide"}],
                "children": [{"id": 16234, "chebi_accession": "CHEBI:16234", "name": "hydroxide"}],
                "is_conjugate_base_of": [
                    {
                        "id": 15377,
                        "chebi_accession": "CHEBI:15377",
                        "definition": "An oxygen hydride consisting of an oxygen atom that is covalently bonded to two hydrogen atoms",
                        "name": "water",
                    }
                ],
                "suggestions": [
                    {"input": "OH-", "weight": 5},
                    {"input": "HYDROXIDE ION", "weight": 5},
                    {"input": "OH−", "weight": 5},
                    {"input": "HO-", "weight": 5},
                    {"input": "Hydroxide ion", "weight": 5},
                    {"input": "hydroxide", "weight": 10},
                ],
            },
        },
        {
            "_index": f"{index._name}",
            "_id": 29412,
            "_source": {
                "chebi_accession": "CHEBI:29412",
                "name": "oxonium",
                "ascii_name": "oxonium",
                "stars": 3,
                "default_structure": 3877,
                "database_references": ["Gmelin:141", "MolBase:1646", "CAS:13968-08-6"],
                "modified_on": "23 Jul 2008",
                "formula": "H3O",
                "inchi": "InChI=1S/H2O/h1H2/p+1",
                "mass": 15.999,
                "inchikey": "XLYOFNOQVPJJNP-UHFFFAOYSA-O",
                "smiles": "[H][O+]([H])[H]",
                "monoisotopicmass": 15.99437,
                "charge": 1,
                "synonyms": [
                    {"name": "Hydronium ion", "scope": "related", "database_reference": "ChemIDplus"},
                    {"name": "H3O+", "scope": "related", "database_reference": "IUPAC"},
                    {"name": "oxonium", "scope": "exact", "database_reference": "IUPAC", "type": "IUPAC_NAME"},
                    {
                        "name": "trihydridooxygen(1+)",
                        "scope": "exact",
                        "database_reference": "IUPAC",
                        "type": "IUPAC_NAME",
                    },
                    {"name": "Hydronium cation", "scope": "related", "database_reference": "NIST_Chemistry_WebBook"},
                    {"name": "aquahydrogen(1+)", "scope": "exact", "database_reference": "IUPAC", "type": "IUPAC_NAME"},
                    {"name": "oxidanium", "scope": "exact", "database_reference": "IUPAC", "type": "IUPAC_NAME"},
                    {"name": "[OH3]+", "scope": "related", "database_reference": "MolBase"},
                ],
                "structures": [3877],
                "parents": [{"id": 29412, "chebi_accession": "CHEBI:29412", "name": "oxonium"}],
                "children": [{"id": 29412, "chebi_accession": "CHEBI:29412", "name": "oxonium"}],
                "is_conjugate_acid_of": [
                    {
                        "id": 15377,
                        "chebi_accession": "CHEBI:15377",
                        "definition": "An oxygen hydride consisting of an oxygen atom that is covalently bonded to two hydrogen atoms",
                        "name": "water",
                    }
                ],
                "suggestions": [
                    {"input": "H3O+", "weight": 5},
                    {"input": "[OH3]+", "weight": 5},
                    {"input": "Hydronium cation", "weight": 5},
                    {"input": "Hydronium ion", "weight": 5},
                    {"input": "oxonium", "weight": 10},
                ],
            },
        },
        {
            "_id": 1230,
            "_index": f"{index._name}",
            "_source": {
                "chebi_accession": "CHEBI:1230",
                "name": "sophorose",
                "ascii_name": "sophorose",
                "stars": 3,
                "default_structure": 3125659,
                "wurcs": "WURCS=2.0/2,2,1/[a2122h-1x_1-5][a2122h-1b_1-5]/1-2/a2-b1",
                "definition": "A glycosylglucose that is <small>D</small>-glucopyranose attached to a β-<small>D</small>-glucopyranosyl unit at position 2 via a glycosidic linkage.",
                "database_references": [
                    "CAS:20429-79-2",
                    "PMID:14469205",
                    "KEGG:C08250",
                    "KNApSAcK:C00001149",
                    "PMID:24655731",
                    "MetaCyc:CPD-13242",
                    "PMID:39061",
                    "Wikipedia:Sophorose",
                ],
                "modified_on": "23 May 2020",
                "monoisotopicmass": 342.11621,
                "smiles": "OC[C@H]1O[C@@H](O[C@H]2C(O)O[C@H](CO)[C@@H](O)[C@@H]2O)[C@H](O)[C@@H](O)[C@@H]1O",
                "charge": 0,
                "mass": 342.297,
                "formula": "C12H22O11",
                "inchikey": "HIWPGCMGAMJNRG-RTPHMHGBSA-N",
                "inchi": "InChI=1S/C12H22O11/c13-1-3-6(16)8(18)10(11(20)21-3)23-12-9(19)7(17)5(15)4(2-14)22-12/h3-20H,1-2H2/t3-,4-,5-,6-,7+,8+,9-,10-,11?,12+/m1/s1",
                "synonyms": [
                    {"name": "Sophorose", "scope": "related", "database_reference": "KEGG_COMPOUND"},
                    {
                        "name": "2-<em>O</em>-β-<small>D</small>-glucopyranosyl-<small>D</small>-glucose",
                        "scope": "related",
                        "database_reference": "MetaCyc",
                    },
                    {
                        "name": "β-<small>D</small>-Glc-(1→2)-<small>D</small>-Glc",
                        "scope": "related",
                        "database_reference": "ChEBI",
                    },
                    {
                        "name": "2-O-beta-D-Glucopyranosyl-D-glucopyranose",
                        "scope": "related",
                        "database_reference": "ChEBI",
                    },
                    {
                        "name": "β-<small>D</small>-glucopyranosyl-(1→2)-<small>D</small>-glucopyranose",
                        "scope": "related",
                        "database_reference": "MetaCyc",
                    },
                    {
                        "name": "2-<em>O</em>-β-<small>D</small>-glucopyranosyl-<small>D</small>-glucopyranose",
                        "scope": "exact",
                        "database_reference": "IUPAC",
                        "type": "IUPAC_NAME",
                    },
                    {"name": "sophorose", "scope": "related", "database_reference": "UniProt"},
                    {
                        "name": "2-O-beta-D-Glucopyranosyl-D-glucose",
                        "scope": "related",
                        "database_reference": "ChemIDplus",
                    },
                ],
                "structures": [1973, 3125659],
                "parents": [
                    {
                        "id": 1230,
                        "chebi_accession": "CHEBI:1230",
                        "definition": "A glycosylglucose that is <small>D</small>-glucopyranose attached to a β-<small>D</small>-glucopyranosyl unit at position 2 via a glycosidic linkage.",
                        "name": "sophorose",
                    }
                ],
                "children": [
                    {
                        "id": 1230,
                        "chebi_accession": "CHEBI:1230",
                        "definition": "A glycosylglucose that is <small>D</small>-glucopyranose attached to a β-<small>D</small>-glucopyranosyl unit at position 2 via a glycosidic linkage.",
                        "name": "sophorose",
                    }
                ],
                "suggestions": [
                    {"input": "Sophorose", "weight": 5},
                    {"input": "2-<em>O</em>-β-<small>D</small>-glucopyranosyl-<small>D</small>-glucose", "weight": 5},
                    {"input": "β-<small>D</small>-Glc-(1→2)-<small>D</small>-Glc", "weight": 5},
                    {"input": "2-O-beta-D-Glucopyranosyl-D-glucopyranose", "weight": 5},
                    {"input": "β-<small>D</small>-glucopyranosyl-(1→2)-<small>D</small>-glucopyranose", "weight": 5},
                    {"input": "sophorose", "weight": 5},
                    {"input": "2-O-beta-D-Glucopyranosyl-D-glucose", "weight": 5},
                    {"input": "sophorose", "weight": 10},
                ],
            },
        },
    ]


def pytest_sessionfinish(session, exitstatus):
    """Delete the testing index in Elasticsearch once all unit tests finish"""
    if get_elasticsearch_connection().indices.exists(index=index_name):
        Index(index_name, using=get_elasticsearch_connection()).delete()
