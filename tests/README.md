## About unit tests

### General information

- A fake ontology saved in `tests/mock_files/chebi.owl.gz` was created  to test the mapping process. It has 15 entries, divided in 12 compounds
and 3 definition roles to perform a classification role using three categories: 
  - `chemical_role`: CHEBI:51086
  - `biological_role`: CHEBI:51086
  - `application`: CHEBI:33232

- There are three compounds that should not be inserted in the Elasticsearch database, 2 deprecated and one `1_STAR` compound. 
- There are three TSV files in `tests/mock_files/` folder, each file was generated using the queries located in `/sql` for each
entity id presents into `chebi_owl.gz`.
