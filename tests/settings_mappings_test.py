import pytest

from dataclasses import dataclass, field
from elasticsearch_dsl.analysis import Normalizer, Analyzer, CharFilter

from indexation.settings_mappings import (
    curie_second_part_filter,
    curie_first_part_filter,
    inchikey_first_part_filter,
    inchikey_second_part_filter,
    keyword_greek_analyzer,
    keyword_html_analyzer,
    keyword_lowercase_analyzer,
    keyword_greek_lowercase_analyzer,
    keyword_alphanumeric_without_space_analyzer,
    keyword_alphanumeric_without_space_lowercase_analyzer,
    small_english_greek_lowercase_analyzer,
    english_greek_lowercase_analyzer,
)

from tests.markers import require_es_connection


@dataclass
class AnalysisData:
    es_analysis: Normalizer | Analyzer = None
    transformed_text: list[str] = field(default_factory=list)
    stream_text: list[str] = field(
        default_factory=lambda: [
            "CHEBI:15377",
            "Water",
            "OBXVUUWPKYYYBL-NCYKPQTJSA-N",
            "CAS:7732-18-5",
            "KEGG:D00000001",
            "UBERON:223000",
            "Reaxys:123a.65b.7",
        ]
    )


# For analyzers that uses tokenizer='keyword' we are gonna set other data test.
keyword_analyzer_stream_text = [
    "[<em>P</em>-4)-α-<small>D</small>-Glc<em>p</em>NAc-(1-<em>P</em>-4)-α-<small>D</small>-Glc<em>p</em>NAc-(1-<em>P</em>-4)-α-<small>D</small>-Glc<em>p</em>NAc-(1-<em>P</em>-4)-α-<small>D</small>-Glc<em>p</em>NAc-(1-<em>P</em>-4)-α-<small>D</small>-Glc<em>p</em>NAc-(1-<em>P</em>-4)-α-<small>D</small>-Glc<em>p</em>NAc-(1-]",
    "β-<small>D</small>-Glc<em>p</em>A-(1→4)-β-<small>D</small>-Glc<em>p</em>OCH<small><sub>2</small></sub>CH<small><sub>2</small></sub>NH<small><sub>2</small></sub>",
    "(<sup><small>18</small></sup>O)water",
]

# Analyzer that does not have a tokenizer='keyword'. Look the aluminium sulfate—water (1/18) name which have the special dash (—)
generic_analyzer_stream_text = keyword_analyzer_stream_text + [
    "water blue",
    "aluminium sulfate—water (1/18)",
    "A dTDP-sugar having 3-amino-3,4,6-trideoxy-α-<small>D</small>-glucose as the sugar component.",
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's "
    "standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make "
    "a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, "
    "remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing "
    "Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions "
    "of Lorem Ipsum.",
]


@pytest.mark.parametrize(
    "analysis_data",
    [
        # Normalizers test data (Comment because this test is not working in Elasticsearch 7.5, but in Elasticsearch >= 7.15
        # Once we update the Elasticsearch version we can uncomment this. In the meantime we are gonna test the
        # char_filters used by the normalizers.
        # AnalysisData(es_analysis=alphanumeric_without_space_lowercase_normalizer,
        #                transformed_text=sorted(['chebi15377', 'water', 'obxvuuwpkyyyblncykpqtjsan', 'cas7732185', 'keggd00000001', 'uberon223000', 'reaxys123a65b7'])),
        #
        # AnalysisData(es_analysis=curie_alphanumeric_second_part_normalizer,
        #                transformed_text=sorted(['chebi15377', 'water', 'obxvuuwpkyyyblncykpqtjsan', '7732185', 'd00000001', '223000', '123a65b7'])),
        #
        # AnalysisData(es_analysis=curie_second_part_normalizer,
        #                transformed_text=sorted(['chebi:15377', 'water', 'obxvuuwpkyyybl-ncykpqtjsa-n', '7732-18-5', 'd00000001', '223000', '123a.65b.7'])),
        # AnalysisData(es_analysis=curie_first_part_normalizer,
        #                transformed_text=sorted(['chebi:15377', 'water', 'obxvuuwpkyyybl-ncykpqtjsa-n', 'cas', 'kegg', 'uberon', 'reaxys'])),
        # AnalysisData(es_analysis=lowercase_normalizer,
        #                transformed_text=sorted(['chebi:15377', 'water', 'obxvuuwpkyyybl-ncykpqtjsa-n', 'cas:7732-18-5', 'kegg:d00000001', 'uberon:223000', 'reaxys:123a.65b.7'])),
        # Charfilters used by the normnalizers, once the Elasticsearch instance is updated, we can drop this and let the normalizers test cases created above.
        AnalysisData(
            es_analysis=curie_first_part_filter,
            transformed_text=sorted(
                ["CHEBI:15377", "Water", "OBXVUUWPKYYYBL-NCYKPQTJSA-N", "CAS", "KEGG", "UBERON", "Reaxys"]
            ),
        ),
        AnalysisData(
            es_analysis=curie_second_part_filter,
            transformed_text=sorted(
                ["15377", "Water", "OBXVUUWPKYYYBL-NCYKPQTJSA-N", "7732-18-5", "D00000001", "223000", "123a.65b.7"]
            ),
        ),
        AnalysisData(
            es_analysis=inchikey_first_part_filter,
            transformed_text=sorted(
                [
                    "CHEBI:15377",
                    "Water",
                    "OBXVUUWPKYYYBL",
                    "CAS:7732-18-5",
                    "KEGG:D00000001",
                    "UBERON:223000",
                    "Reaxys:123a.65b.7",
                ]
            ),
        ),
        AnalysisData(
            es_analysis=inchikey_second_part_filter,
            transformed_text=sorted(
                [
                    "CHEBI:15377",
                    "Water",
                    "OBXVUUWPKYYYBL-NCYKPQTJSA",
                    "CAS:7732-18-5",
                    "KEGG:D00000001",
                    "UBERON:223000",
                    "Reaxys:123a.65b.7",
                ]
            ),
        ),
        # Keyword Analyzers test data
        AnalysisData(
            es_analysis=keyword_greek_analyzer,
            stream_text=keyword_analyzer_stream_text,
            transformed_text=sorted(
                [
                    "[P-4)-alpha-D-GlcpNAc-(1-P-4)-alpha-D-GlcpNAc-(1-P-4)-alpha-D-GlcpNAc-(1-P-4)-alpha-D-GlcpNAc-(1-P-4)-alpha-D-GlcpNAc-(1-P-4)-alpha-D-GlcpNAc-(1-]",
                    "beta-D-GlcpA-(1→4)-beta-D-GlcpOCH2CH2NH2",
                    "(18O)water",
                ]
            ),
        ),
        AnalysisData(
            es_analysis=keyword_html_analyzer,
            stream_text=keyword_analyzer_stream_text,
            transformed_text=sorted(
                [
                    "[P-4)-α-D-GlcpNAc-(1-P-4)-α-D-GlcpNAc-(1-P-4)-α-D-GlcpNAc-(1-P-4)-α-D-GlcpNAc-(1-P-4)-α-D-GlcpNAc-(1-P-4)-α-D-GlcpNAc-(1-]",
                    "β-D-GlcpA-(1→4)-β-D-GlcpOCH2CH2NH2",
                    "(18O)water",
                ]
            ),
        ),
        AnalysisData(
            es_analysis=keyword_lowercase_analyzer,
            stream_text=keyword_analyzer_stream_text,
            transformed_text=sorted(
                [
                    "[p-4)-α-d-glcpnac-(1-p-4)-α-d-glcpnac-(1-p-4)-α-d-glcpnac-(1-p-4)-α-d-glcpnac-(1-p-4)-α-d-glcpnac-(1-p-4)-α-d-glcpnac-(1-]",
                    "β-d-glcpa-(1→4)-β-d-glcpoch2ch2nh2",
                    "(18o)water",
                ]
            ),
        ),
        AnalysisData(
            es_analysis=keyword_greek_lowercase_analyzer,
            stream_text=keyword_analyzer_stream_text,
            transformed_text=sorted(
                [
                    "[p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-]",
                    "beta-d-glcpa-(1→4)-beta-d-glcpoch2ch2nh2",
                    "(18o)water",
                ]
            ),
        ),
        AnalysisData(
            es_analysis=keyword_alphanumeric_without_space_analyzer,
            stream_text=keyword_analyzer_stream_text,
            transformed_text=sorted(
                [
                    "P4DGlcpNAc1P4DGlcpNAc1P4DGlcpNAc1P4DGlcpNAc1P4DGlcpNAc1P4DGlcpNAc1",
                    "DGlcpA14DGlcpOCH2CH2NH2",
                    "18Owater",
                ]
            ),
        ),
        AnalysisData(
            es_analysis=keyword_alphanumeric_without_space_lowercase_analyzer,
            stream_text=keyword_analyzer_stream_text,
            transformed_text=sorted(
                [
                    "p4dglcpnac1p4dglcpnac1p4dglcpnac1p4dglcpnac1p4dglcpnac1p4dglcpnac1",
                    "dglcpa14dglcpoch2ch2nh2",
                    "18owater",
                ]
            ),
        ),
    ],
)
@require_es_connection
def test_keyword_analyzers_and_normalizers(index, analysis_data: AnalysisData):
    analysis_information = [
        index.analyze(
            body={
                "text": st,
                "normalizer"
                if isinstance(analysis_data.es_analysis, Normalizer)
                else "char_filter"
                if isinstance(analysis_data.es_analysis, CharFilter)
                else "analyzer": [analysis_data.es_analysis]
                if isinstance(analysis_data.es_analysis, CharFilter)
                else analysis_data.es_analysis,
            }
        )
        for st in analysis_data.stream_text
    ]
    tokens = sorted(analysis.get("tokens")[0].get("token") for analysis in analysis_information)

    assert all(
        token == transformed for (token, transformed) in zip(tokens, analysis_data.transformed_text, strict=True)
    )


@pytest.mark.parametrize(
    "analysis_data",
    [
        AnalysisData(
            es_analysis=small_english_greek_lowercase_analyzer,
            stream_text=generic_analyzer_stream_text,
            transformed_text=sorted(
                [
                    "(1/18)",
                    "(18o)water",
                    "1500s,",
                    "3-amino-3,4,6-trideoxy-alpha-d-glucos",
                    "[p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-p-4)-alpha-d-glcpnac-(1-]",
                    "a",
                    "aluminium",
                    "been",
                    "beta-d-glcpa-(1→4)-beta-d-glcpoch2ch2nh2",
                    "blue",
                    "component.",
                    "dtdp-sugar",
                    "dummi",
                    "dummi",
                    "ever",
                    "ha",
                    "have",
                    "industry'",
                    "industry.",
                    "ipsum",
                    "ipsum",
                    "lorem",
                    "lorem",
                    "print",
                    "simpli",
                    "sinc",
                    "standard",
                    "sugar",
                    "sulfat",
                    "text",
                    "text",
                    "typeset",
                    "water",  # this water is due to 'water blue'
                    "water",  # this one because 'aluminium sulfate—water (1/18)'
                    "when",
                ]
            ),
        ),  # Lorem ipsum text will be limited until 20 tokens, because of small_limit_length_filter
        AnalysisData(
            es_analysis=english_greek_lowercase_analyzer,
            stream_text=generic_analyzer_stream_text,
            transformed_text=sorted(
                [
                    "1/18",
                    "18owater",
                    "1500s,",
                    "1960",
                    "3-amino-3,4,6-trideoxy-alpha-d-glucos",
                    "[p-4-alpha-d-glcpnac-1-p-4-alpha-d-glcpnac-1-p-4-alpha-d-glcpnac-1-p-4-alpha-d-glcpnac-1-p-4-alpha-d-glcpnac-1-p-4-alpha-d-glcpnac-1-]",
                    "a",
                    "aldu",
                    "also",
                    "aluminium",
                    "been",
                    "beta-d-glcpa-1→4-beta-d-glcpoch2ch2nh2",
                    "blue",
                    "book.",
                    "centuries,",
                    "component.",
                    "contain",
                    "desktop",
                    "dtdp-sugar",
                    "dummi",
                    "dummi",
                    "electron",
                    "essenti",
                    "ever",
                    "five",
                    "gallei",
                    "ha",
                    "ha",
                    "have",
                    "includ",
                    "industry'",
                    "industry.",
                    "ipsum",
                    "ipsum",
                    "ipsum",
                    "ipsum.",
                    "it",
                    "it",
                    "leap",
                    "letraset",
                    "like",
                    "lorem",
                    "lorem",
                    "lorem",
                    "lorem",
                    "make",
                    "more",
                    "onli",
                    "pagemak",
                    "passages,",
                    "popularis",
                    "print",
                    "printer",
                    "publish",
                    "recent",
                    "releas",
                    "remain",
                    "scrambl",
                    "sheet",
                    "simpli",
                    "sinc",
                    "softwar",
                    "specimen",
                    "standard",
                    "sugar",
                    "sulfat",
                    "surviv",
                    "text",
                    "text",
                    "took",
                    "type",
                    "type",
                    "typeset",
                    "typesetting,",
                    "unchanged.",
                    "unknown",
                    "version",
                    "water",
                    "water",
                    "when",
                ]
            ),
        ),
    ],
)
@require_es_connection
def test_generic_analyzer(index, analysis_data: AnalysisData):
    analysis_information = [
        index.analyze(body={"text": st, "analyzer": analysis_data.es_analysis}) for st in analysis_data.stream_text
    ]

    tokens = sorted(tokens.get("token") for analysis in analysis_information for tokens in analysis.get("tokens"))
    assert tokens == analysis_data.transformed_text
