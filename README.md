# Table of content

<!-- TOC -->
* [Table of content](#table-of-content)
* [Indexation process to the ChEBI Database](#indexation-process-to-the-chebi-database)
  * [Strategy](#strategy)
  * [Run the process locally](#run-the-process-locally)
  * [Run using docker](#run-using-docker)
<!-- TOC -->

# Indexation process to the ChEBI Database

This repository has the code to index the ChEBI's information into Elasticsearch. It is used data from both, the ChEBI Ontology and the PostgreSQL Database.
The most part of the information is extracted from the ChEBI Ontology, if data is not available in the ontology, then we use the ChEBI Postgres database.

## Strategy

In order to be up-to-date with last changes made by the curators, data will be updated everyday following this strategy:

1. **Creation of a daily index**: Every day we are going to create a new index keeping this pattern: `chebi-<ENV>-compounds-<ddmmYYYY>`, 
being <ENV> the environment of the data, its possible values are: `dev`, `stage` or `prod` and, <ddmmYYYY> the current date format.
2. **Bulk the information to the new index**: Once index was created, we need to re-create all the documents there.
3. **Assign an alias to the new index**: Alias is assigned taking into account the data environment, possible values are: `chebi-dev-compounds`, `chebi-stage-compounds` and `chebi-prod-compounds` 
4. **Delete the previous index**: The previous index (from yesterday) is deleted, the idea is that we are going to have only one index alive.

So, for example, if today is August 17th. 2023 and we are going to generate data from production, then this process will execute these steps:

1. Create the index `chebi-prod-compounds-17082023`.
2. Create all the documents in the above index.
3. Assign the alias `chebi-prod-compounds` to the index `chebi-prod-compounds-17082023`.
4. Delete the index `chebi-prod-compounds-16082023`, because in our example yesterday was August 16th 2023. Actually, the process will try to delete
all past indices (if exist more than one) except the current one.

## Run the process locally

1. Clone the repo: `git clone https://gitlab.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-elasticsearch`
2. Go to the folder: `cd chebi-elasticsearch`
3. Make a copy of `config.template.yml` and called it just `config.yml`: `cp config.template.yml config.yml`
4. Edit the file `config.yml` writing the correct connections credentials to both: PostgreSQL and Elasticsearch databases
5. Execute the process: `python indexation_process.py`

## Run using docker

1. Clone the repo: `git clone https://gitlab.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-elasticsearch`
2. Go to the folder: `cd chebi-elasticsearch`
3. Create a few environment variables related to the credentials databases:
   1. `PG_HOST_DEV`: Host from postgres database.
   2. `PG_PORT_DEV`: Port from postgres database.
   3. `PG_USER_DEV`: User from postgres database.
   4. `PG_PASSWORD_DEV`: Password from postgres database.
   5. `PG_DBNAME_DEV`: Database name from postgres database.
   6. `CHEBI_ELASTIC_HOST`: Elasticsearch host
   7. `CHEBI_ELASTIC_PORT`: Elasticsearch port
   8. `CHEBI_ELASTIC_PASSWORD`: Elasticsearch password
   9. `CHEBI_ELASTIC_ALIAS`: Elasticsearch alias (example: `chebi-dev-compounds`)
   10. `CHEBI_ELASTIC_TIMEOUT`: Elasticsearch timeout (example: 20000)
   11. `ONTOLOGY_PATH_DAILY_UPDATE`: Path to the ontology (`chebi.owl.gz` file). For example https://ftp.ebi.ac.uk/pub/databases/chebi-2/ontology/nightly/ or a local folder
4. Execute the process: `docker compose up`