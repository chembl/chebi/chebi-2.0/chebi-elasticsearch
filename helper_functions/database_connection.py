import sqlalchemy
from helper_functions.read_configuration import read_configuration
from elasticsearch_dsl import connections
from elasticsearch import Elasticsearch


def get_elasticsearch_connection() -> Elasticsearch:
    configuration_elasticsearch = read_configuration().get("databases").get("elasticsearch")
    user = configuration_elasticsearch.get("user")
    password = configuration_elasticsearch.get("password")
    host = configuration_elasticsearch.get("host")
    port = configuration_elasticsearch.get("port")
    timeout = int(configuration_elasticsearch.get("timeout"))

    return connections.create_connection(hosts=f"{host}:{port}", timeout=timeout, http_auth=(user, password))


def get_pg_connection() -> sqlalchemy.Engine:
    configuration_database_pg = read_configuration().get("databases").get("postgres")
    user = configuration_database_pg.get("user")
    password = configuration_database_pg.get("password")
    host = configuration_database_pg.get("host")
    port = configuration_database_pg.get("port")
    db_name = configuration_database_pg.get("dbname")
    return sqlalchemy.create_engine(f"postgresql://{user}:{password}@{host}:{port}/{db_name}")
