import logging
import yaml

from pathlib import Path

parent_folder = Path(__file__).parents[1]
logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO)


def read_configuration():
    with open(f"{parent_folder}/config.yml", "r") as config_file:
        config_information = yaml.safe_load(config_file)
    return config_information
