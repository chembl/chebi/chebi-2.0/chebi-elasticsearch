import logging
import pronto as pt
import warnings
from helper_functions.read_configuration import read_configuration
from enum import StrEnum

logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=logging.NOTSET)


class OntologyVariants(StrEnum):
    LITE = "_lite"
    CORE = "_core"
    FULL = ""
    TEST = "_for_testing"


class AnnotationPropertyEquivalences(StrEnum):
    """ChEBI ontology uses chemrof for annotation properties. We are assigning a simple value used by elasticsearch
    to create the documents.

    <value_in_chemrof> = <simple_value_used_by_elasticsearch>
    """

    generalized_empirical_formula = "formula"
    inchi_string = "inchi"
    inchi_key_string = "inchikey"
    wurcs_representation = "wurcs"
    smiles_string = "smiles"
    charge = "charge"
    mass = "mass"
    monoisotopic_mass = "monoisotopicmass"


def get_ontology(variant: OntologyVariants, gzip: bool = True) -> pt.ontology.Ontology:
    warnings.filterwarnings("ignore", category=pt.warnings.NotImplementedWarning, module="pronto")
    configuration_ontology = read_configuration().get("ontology")
    ontology_file = f"chebi{variant.value}.owl.gz" if gzip else f"chebi{variant}.owl"
    logging.info(f"> Loading {variant.name} ontology: {ontology_file}")
    path = f'{configuration_ontology.get("path")}/{ontology_file}'
    ontology_object = pt.Ontology(path)
    logging.info(f"> {ontology_file} loaded successfully")
    return ontology_object


ontology = get_ontology(OntologyVariants.FULL)
# For loading subset of the ontology: ontology = get_ontology(OntologyVariants.TEST, gzip=False)
