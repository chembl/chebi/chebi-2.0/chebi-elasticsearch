from pathlib import Path
import logging

parent_folder = Path(__file__).parents[1]
SQL_FOLDER = parent_folder / "sql"

logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO)


def get_sql_sentence(name):
    # Get the content of a sql file based on its file name.
    sql_path = Path(f"{SQL_FOLDER}/{name}")
    sql_names = [n for n in get_sql_files()]
    if name in sql_names:
        sql_file = open(sql_path.as_posix(), mode="r")
        sql = sql_file.read()
        sql_file.close()
        return sql
    else:
        msg_error = f">{name} does not match with available sql files : {','.join(sql_names)}"
        logging.error(msg_error)
        raise ValueError(msg_error)


def get_sql_files():
    return [path.name for path in SQL_FOLDER.glob("*.sql")]
