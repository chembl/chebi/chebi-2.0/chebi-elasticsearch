import abc
import elasticsearch
import logging
import time

from datetime import datetime
from elasticsearch.helpers import streaming_bulk
from elasticsearch_dsl import Index
from helper_functions.ontology_functions import ontology
from helper_functions.database_connection import get_elasticsearch_connection
from indexation.documents import get_compounds
from indexation.data_from_postgres import get_data_from_postgres
from indexation.entities import Compound, IndexAlias

logging.getLogger("elasticsearch").setLevel(logging.WARNING)
logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO)


class IndexationProcess(abc.ABC):
    """IndexationProcess is a class which defines a common structure for the ChEBI's indexation process:
        1. Creation of the new index
        2. Migration of the data into the new index
        3. Assign an alias to the new index, and remove it from the current index, which will become the past one.
        4. Delete the previous index.
    It is created an IndexTemplate taking into account the Compound class, this template is created before building the index,
    therefore, each new index will follow the features defined in the template.
    """

    @abc.abstractmethod
    def create_new_index(self, connection: elasticsearch.Elasticsearch):
        pass

    @abc.abstractmethod
    def migrate(self, connection: elasticsearch.Elasticsearch):
        pass

    @abc.abstractmethod
    def assign_alias(self, connection: elasticsearch.Elasticsearch):
        pass

    def delete_previous_index(self, connection: elasticsearch.Elasticsearch):
        pass

    def run(self, connection: elasticsearch.Elasticsearch):
        """Execute the indexation in order"""
        self.create_new_index(connection)
        self.migrate(connection)
        self.assign_alias(connection)
        self.delete_previous_index(connection)


class IndexationChEBI(IndexationProcess):
    """IndexationChEBI is a class which implements the IndexationProcess interface. Here is programmed all logic behind
    the indexation."""

    def __init__(self) -> None:
        """Create an index template using as a base the Compound class, this is the first thing we want to do because
        all the new indexes created daily will use the features from the template."""
        self.alias = Compound.Index.alias()
        self.pattern = Compound.Index.pattern()
        self.next_index = Compound.Index.next_index()
        self.previous_index = Compound.Index.previous_index()

    def create_index_template(self):
        if self.alias in (allowed_aliases := list(i.value for i in IndexAlias)):
            logging.info(f"> Creating index template {self.pattern}")
            self.chebi_index_template = Compound._index.as_template(self.alias, self.pattern)
            self.chebi_index_template.save()
            logging.info(f"> Index {self.next_index} created.")
        else:
            raise KeyError(
                f"Alias {self.alias} is not allowed, these are the allowed aliases: {allowed_aliases}, check your config.yml file"
            )

    def create_new_index(self, connection: elasticsearch.Elasticsearch) -> None:
        """This function create a new index in elastic search using an index template created previously,
        the name is calculated based on the current date, example: chebi-dev-compounds-ddmmYYYY
        If the index already exist, then this function tries to re-create it."""
        try:
            self.create_index_template()
            logging.info(f"> Creating a new index: {self.next_index}")
            connection.indices.create(index=self.next_index)
            logging.info(f"> Index {self.next_index} created.")
        except elasticsearch.exceptions.RequestError as exception:
            if exception.error == "resource_already_exists_exception":
                logging.warning(f"Index {self.next_index} already exists, recreating it.")
                connection.indices.delete(index=self.next_index)
                connection.indices.create(index=self.next_index)
            else:
                raise exception

    def migrate(self, connection: elasticsearch.Elasticsearch) -> None:
        """Create all the documents in the new index. Compounds with 1_STAR are not migrated, as well as
        obsoletes compounds. Migration is performed by chunks=500. If an error happens in this step, we delete the next_index
        to be migrated."""
        try:
            total = len(
                list(filter(lambda term: not term.obsolete and "chebi/1:STAR" not in term.subsets, ontology.terms()))
            )
            created = 0
            logging.info(f'> Starting migration process to "{self.next_index}" index')
            compound_info_from_pg = get_data_from_postgres()
            compound_records = get_compounds(compound_info_from_pg)

            for ok, _ in streaming_bulk(
                client=connection, actions=compound_records, index=self.next_index, chunk_size=100
            ):
                created += ok
                logging.info(f"\tInserting document {created}/{total}")

            logging.info(f"> Has been created {created} documents in {self.next_index} index")
        except Exception as ex:
            logging.error(f"> An error has happened in the migrate step {ex.args}. Deleting progress")
            connection.indices.delete(index=self.next_index)
            raise

    def assign_alias(self, connection: elasticsearch.Elasticsearch) -> None:
        """Once all the documents have been migrated in the new index, it is important remove the alias from
        the current index (which has become the past index), to the next one."""

        # Using this for-loop we asure that we are deleting the last index available. For example, if we have this
        # scenario:
        # 1. Index on August 23 2023 (chebi-dev-compounds-23082023) Migrated successfully
        # 2. Index on August 24 2023 (chebi-dev-compounds-24082023) Failed
        # 3. Index on August 25 2023 (chebi-dev-compounds-25082023) Failed
        # Based on the above, we need to create the index on August 26 2023 (chebi-dev-compounds-26082023):
        # We need to obtain the last index created successfully, which is NOT the self.previous_index, but the index on
        # August 23 2023 (chebi-dev-compounds-23082023).
        for previous_index in connection.indices.get(index=self.pattern):
            if previous_index != self.next_index:
                logging.info(f"> Removing alias {self.alias} from {previous_index}")
                connection.indices.update_aliases(
                    body={"actions": [{"remove": {"alias": self.alias, "index": previous_index}}]}
                )

        connection.indices.update_aliases(body={"actions": [{"add": {"alias": self.alias, "index": self.next_index}}]})

        logging.info(f"> Alias {self.alias} assigned to {self.next_index} correctly")

    def delete_previous_index(self, connection: elasticsearch.Elasticsearch):
        """Delete the previous index (or indices) related to our index template pattern if it (they) exists.
        We should have only one index with one alias assigned."""
        logging.info("> Deleting previous index")
        for index in connection.indices.get(index=self.pattern):
            if index != self.next_index:
                Index(index).delete()
                logging.info(f"> Index {index} deleted successfully")


def main():
    connection = get_elasticsearch_connection()
    try:
        logging.info(f'>>> Indexation process for {datetime.now().strftime("%B %d, %Y")} <<<')
        indexation_process = IndexationChEBI()
        indexation_process.run(connection)
        logging.info("ChEBI Indexation process finished successfully")
    except Exception as ex:
        logging.error(f"An uncontrolled exception has happened {ex}", exc_info=True)
        connection.transport.close()
        raise ex
    finally:
        logging.info("> Closing the elasticsearch connection")
        connection.transport.close()
        logging.info("> Connection closed successfully")


if __name__ == "__main__":
    start_time = time.time()
    main()
    logging.info(f"> ETL process finished, execution time: {time.time() - start_time}")
