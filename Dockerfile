FROM python:3.11-slim-buster
ENV PYTHONUNBUFFERED 1
ARG PG_HOST
ARG PG_PORT
ARG PG_USER
ARG PG_PASSWORD
ARG PG_DBNAME

ARG CHEBI_ELASTIC_HOST
ARG CHEBI_ELASTIC_PORT
ARG CHEBI_ELASTIC_USER
ARG CHEBI_ELASTIC_PASSWORD
ARG CHEBI_ELASTIC_ALIAS
ARG CHEBI_ELASTIC_TIMEOUT
ARG ONTOLOGY_PATH

ENV PG_PORT $PG_PORT
ENV PG_USER $PG_USER
ENV PG_PASSWORD $PG_PASSWORD
ENV PG_DBNAME $PG_DBNAME
ENV PG_HOST $PG_HOST

ENV CHEBI_ELASTIC_HOST $CHEBI_ELASTIC_HOST
ENV CHEBI_ELASTIC_PORT $CHEBI_ELASTIC_PORT
ENV CHEBI_ELASTIC_USER $CHEBI_ELASTIC_USER
ENV CHEBI_ELASTIC_PASSWORD $CHEBI_ELASTIC_PASSWORD
ENV CHEBI_ELASTIC_ALIAS $CHEBI_ELASTIC_ALIAS
ENV CHEBI_ELASTIC_TIMEOUT $CHEBI_ELASTIC_TIMEOUT
ENV ONTOLOGY_PATH $ONTOLOGY_PATH

WORKDIR /chebi-elasticsearch
COPY ./requirements.txt /chebi-elasticsearch
RUN pip install -r requirements.txt
COPY . /chebi-elasticsearch

RUN apt-get update && \
    apt-get install -y wget && \
    wget -qO /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 && \
    chmod a+x /usr/local/bin/yq && \
    cp config.template.yml config.yml

RUN yq -i '.databases.elasticsearch.host = strenv(CHEBI_ELASTIC_HOST) | \
           .databases.elasticsearch.port = strenv(CHEBI_ELASTIC_PORT) | \
           .databases.elasticsearch.user = strenv(CHEBI_ELASTIC_USER) | \
           .databases.elasticsearch.password = strenv(CHEBI_ELASTIC_PASSWORD) | \
           .databases.elasticsearch.alias = strenv(CHEBI_ELASTIC_ALIAS) | \
           .databases.elasticsearch.timeout = strenv(CHEBI_ELASTIC_TIMEOUT) | \
           .databases.postgres.host = strenv(PG_HOST) | \
           .databases.postgres.port = strenv(PG_PORT) | \
           .databases.postgres.user = strenv(PG_USER) | \
           .databases.postgres.password = strenv(PG_PASSWORD) | \
           .databases.postgres.dbname = strenv(PG_DBNAME) | \
           .ontology.path = strenv(ONTOLOGY_PATH) \
            ' config.yml

ENTRYPOINT ["python", "/chebi-elasticsearch/indexation_process.py"]